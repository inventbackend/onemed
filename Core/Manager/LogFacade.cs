﻿/* documentation
 *001 
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Core.Manager
{
    public class LogFacade
    {
        
        public static string InsertLog(string lFunctionName, string lCreatedBy, string lResult, string branchID, string deviceID, string size)
        {
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@Createdby", SqlDbType = SqlDbType.VarChar, Value = lCreatedBy},
                new SqlParameter() {ParameterName = "@TimeStamp", SqlDbType = SqlDbType.DateTime, Value = DateTime.Now},
                new SqlParameter() {ParameterName = "@FunctionName", SqlDbType = SqlDbType.VarChar, Value = lFunctionName},
                new SqlParameter() {ParameterName = "@Result", SqlDbType = SqlDbType.VarChar, Value = lResult},
                new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.VarChar, Value = branchID},
                new SqlParameter() {ParameterName = "@DeviceID", SqlDbType = SqlDbType.VarChar, Value = deviceID},
                new SqlParameter() {ParameterName = "@Size", SqlDbType = SqlDbType.VarChar, Value = size}
            };

            string result = DataFacade.DTSQLVoidCommand("INSERT INTO Log_Service (FunctionName,Timestamp,CreatedBy,Result, BranchID, DeviceID, Size) VALUES (@FunctionName,@Timestamp,@Createdby,@Result,@BranchID, @DeviceID, @Size)", sp);

            return result;
        }

        public static string InsertLogException(string lException, string lQuery, string lKey, string lCreated_by)
        {
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@Exception", SqlDbType = SqlDbType.VarChar, Value = lException},
                new SqlParameter() {ParameterName = "@Query", SqlDbType = SqlDbType.VarChar, Value = lQuery},
                new SqlParameter() {ParameterName = "@Key", SqlDbType = SqlDbType.VarChar, Value = lKey},
                new SqlParameter() {ParameterName = "@Created_by", SqlDbType = SqlDbType.VarChar, Value = lCreated_by}
            };

            string result = DataFacade.DTSQLVoidCommand("INSERT INTO Log_Exception ([Exception],Query,[Key],Created_on, Created_by) VALUES (@Exception,@Query,@Key,GETDATE(),@Created_by)", sp);

            return result;
        }


        public static bool CheckLogLocationReqbyDate(string lDate)
        {
            List<SqlParameter> sp = new List<SqlParameter>()
            {
               new SqlParameter() {ParameterName = "@Date", SqlDbType = SqlDbType.NVarChar, Value = lDate },
            };

            DataTable dtLogLocationReq = Manager.DataFacade.DTSQLCommand(@"SELECT TOP 1 Date
                                                                   FROM Log_LocationRequest 
                                                                   WHERE Date = @Date", sp);
            bool lCheck = false;
            if (dtLogLocationReq.Rows.Count == 0)
            {
                lCheck = true;
            }

            return lCheck;
            //klau true berarti datanya belum ada
        }

        public static List<Model.mdlLogLocationReq> getLog_LocationReq(string lDate)
        {
            var mdlLogLocationReqList = new List<Model.mdlLogLocationReq>();

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@Date", SqlDbType = SqlDbType.NVarChar, Value= lDate },

            };

            DataTable dtLog = DataFacade.DTSQLCommand(@"SELECT TOP 1 Date,RequestCounter From Log_LocationRequest
                                                        WHERE Date = @Date", sp);

            foreach (DataRow row in dtLog.Rows)
            {
                var mdlLogLocationReq = new Model.mdlLogLocationReq();

                mdlLogLocationReq.Date = row["Date"].ToString();
                mdlLogLocationReq.RequestCounter = row["RequestCounter"].ToString();

                mdlLogLocationReqList.Add(mdlLogLocationReq);
            }

            return mdlLogLocationReqList;
        }
        
    }
}
