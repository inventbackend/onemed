﻿/* documentation
 * 
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Dynamic;
using System.Globalization;
using Core.Model;

namespace Core.Manager
{
    public class PriceFacade
    {
        public static List<Model.mdlRptPriceDetail> LoadPrice(string lBranchID,string lAccount, string lCustomerList, string lProductIDList, string lWeekFrom, string lWeekTo, string lYear)
        {
            Globals.gKey = "LoadPrice";
            var mdlEDWeekList = OSAFacade.GetWeeks(lWeekFrom, lWeekTo, lYear);
            string lParam = string.Empty;

            var mdlRptPriceList = new List<Model.mdlRptPriceDetail>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
                new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
                new SqlParameter() {ParameterName = "@Area", SqlDbType = SqlDbType.NVarChar, Value = lBranchID },
                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                new SqlParameter() {ParameterName = "@Account", SqlDbType = SqlDbType.NVarChar, Value = lAccount },
                new SqlParameter() {ParameterName = "@CustomerList", SqlDbType = SqlDbType.NVarChar, Value = lCustomerList },
                new SqlParameter() {ParameterName = "@ProductList", SqlDbType = SqlDbType.NVarChar, Value = lProductIDList }
            };

            DataTable dt = Manager.DataFacade.GetSP("spReportPriceArea", sp);
            decimal lNormalPrice, lPromoPrice;
            foreach (DataRow dr in dt.Rows)
            {
                var mdlRptPrice = new Model.mdlRptPriceDetail();
                mdlRptPrice.BranchID = dr["BranchID"].ToString();
                mdlRptPrice.ProductID = dr["ProductID"].ToString();
                mdlRptPrice.ProductName = dr["ProductName"].ToString();
                mdlRptPrice.CustomerID = dr["CustomerID"].ToString();
                mdlRptPrice.CustomerName = dr["CustomerName"].ToString(); 
                mdlRptPrice.VisitWeek = dr["VisitWeek"].ToString();

                lNormalPrice = Convert.ToDecimal(dr["NormalPrice"].ToString());
                mdlRptPrice.NormalPrice = Math.Round(lNormalPrice,2).ToString();
                lPromoPrice = Convert.ToDecimal(dr["PromoPrice"].ToString());
                mdlRptPrice.PromoPrice = Math.Round(lPromoPrice, 2).ToString();

                mdlRptPriceList.Add(mdlRptPrice);
                
            }

            return mdlRptPriceList;
        }


        public static List<Model.mdlRptPriceExcel> LoadPriceExcel(string lBranchID, string lAccount, string lCustomerList, string lProductIDList, string lWeekFrom, string lWeekTo, string lYear)
        {
            Globals.gKey = "LoadPriceExcel";
            var mdlEDWeekList = OSAFacade.GetWeeks(lWeekFrom, lWeekTo, lYear);
            string lParam = string.Empty;

            var mdlRptPriceList = new List<Model.mdlRptPriceExcel>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
                new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
                new SqlParameter() {ParameterName = "@Area", SqlDbType = SqlDbType.NVarChar, Value = lBranchID },
                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                new SqlParameter() {ParameterName = "@Account", SqlDbType = SqlDbType.NVarChar, Value = lAccount },
                new SqlParameter() {ParameterName = "@CustomerList", SqlDbType = SqlDbType.NVarChar, Value = lCustomerList },
                new SqlParameter() {ParameterName = "@ProductList", SqlDbType = SqlDbType.NVarChar, Value = lProductIDList }
            };

            DataTable dt = Manager.DataFacade.GetSP("spReportPriceArea", sp);
            decimal lNormalPrice, lPromoPrice;
            foreach (DataRow dr in dt.Rows)
            {
                var mdlRptPrice = new Model.mdlRptPriceExcel();
                mdlRptPrice.BranchID = dr["BranchID"].ToString();
                mdlRptPrice.ProductID = dr["ProductID"].ToString();
                mdlRptPrice.ProductName = dr["ProductName"].ToString();
                mdlRptPrice.CustomerID = dr["CustomerID"].ToString();
                mdlRptPrice.CustomerName = dr["CustomerName"].ToString();
                mdlRptPrice.VisitWeek = dr["VisitWeek"].ToString();

                lNormalPrice = Convert.ToDecimal(dr["NormalPrice"].ToString());
                mdlRptPrice.NormalPrice = Math.Round(lNormalPrice, 2).ToString();
                lPromoPrice = Convert.ToDecimal(dr["PromoPrice"].ToString());
                mdlRptPrice.PromoPrice = Math.Round(lPromoPrice, 2).ToString();

                mdlRptPriceList.Add(mdlRptPrice);

            }

            return mdlRptPriceList;
        }

    }
}
