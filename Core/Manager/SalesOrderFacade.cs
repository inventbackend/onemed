﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Core.Manager
{
    public class SalesOrderFacade : Base.Manager
    {

        public static List<Model.mdlSalesOrderReport> LoadSalesOrderReport(string lBranchID, DateTime lStartDate, DateTime lEndDate, List<string> lEmployeeIDlist)
        {
            string lParam = string.Empty;

            foreach (var lEmployeeID in lEmployeeIDlist)
            {
                if (lParam == "")
                {
                    lParam = lEmployeeID;
                }
                else
                {
                    lParam += "," + lEmployeeID;
                }

            }

            List<SqlParameter> sp = new List<SqlParameter>()
            {
               new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranchID },
               new SqlParameter() {ParameterName = "@EmployeeID", SqlDbType = SqlDbType.NVarChar, Value = lParam },
               new SqlParameter() {ParameterName = "@StartDate", SqlDbType = SqlDbType.DateTime, Value = lStartDate },
               new SqlParameter() {ParameterName = "@EndDate", SqlDbType = SqlDbType.DateTime, Value = lEndDate.AddDays(1) }
            };

            var listSalesOrderReport = new List<Model.mdlSalesOrderReport>();
            DataTable dtSalesOrder = Manager.DataFacade.GetSP("spReportSalesOrder", sp);

            foreach (DataRow row in dtSalesOrder.Rows)
            {
                var mdlSalesOrderReport = new Model.mdlSalesOrderReport();
                mdlSalesOrderReport.SalesOrderID = Convert.ToInt32(row["SalesOrderID"].ToString());
                mdlSalesOrderReport.Date = row["Date"].ToString();
                mdlSalesOrderReport.CustomerID = row["CustomerID"].ToString();
                mdlSalesOrderReport.CustomerName = row["CustomerName"].ToString();
                mdlSalesOrderReport.EmployeeID = row["EmployeeID"].ToString();
                mdlSalesOrderReport.EmployeeName = row["EmployeeName"].ToString();
                mdlSalesOrderReport.PaymentAmount = Convert.ToDecimal(row["PaymentAmount"].ToString());
                mdlSalesOrderReport.Line = Convert.ToInt32(row["Line"].ToString());
                mdlSalesOrderReport.ProductID = row["ProductID"].ToString();
                mdlSalesOrderReport.ProductName = row["ProductName"].ToString();
                mdlSalesOrderReport.Qty = Convert.ToInt32(row["Qty"].ToString());
                mdlSalesOrderReport.UOM = row["UOM"].ToString();
                mdlSalesOrderReport.Price = Convert.ToDecimal(row["Price"].ToString());
                mdlSalesOrderReport.BranchID = row["BranchID"].ToString();

                listSalesOrderReport.Add(mdlSalesOrderReport);
            }
            return listSalesOrderReport;
        }

    }
}
