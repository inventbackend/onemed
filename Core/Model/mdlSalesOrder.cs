﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core.Model
{

    [DataContract]
    public class mdlSalesOrderReport
    {
        [DataMember]
        public int SalesOrderID { get; set; }

        [DataMember]
        public string Date { get; set; }

        [DataMember]
        public string CustomerID { get; set; }

        [DataMember]
        public string CustomerName { get; set; }

        [DataMember]
        public string EmployeeID { get; set; }

        [DataMember]
        public string EmployeeName { get; set; }

        [DataMember]
        public decimal PaymentAmount { get; set; }

        [DataMember]
        public int Line { get; set; }

        [DataMember]
        public string ProductID { get; set; }

        [DataMember]
        public string ProductName { get; set; }

        [DataMember]
        public int Qty { get; set; }

        [DataMember]
        public string UOM { get; set; }

        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public string BranchID { get; set; }
    }

}
