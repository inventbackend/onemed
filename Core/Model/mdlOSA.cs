﻿/* documentation
 *001 
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core.Model
{
    public class mdlRptOSA
    {
        [DataMember]
        public string Weeks { get; set; }

        [DataMember]
        public string Account { get; set; }

        [DataMember]
        public string Percent { get; set; }

        [DataMember]
        public string Customer { get; set; }

        [DataMember]
        public string Product { get; set; }

        [DataMember]
        public string Stock { get; set; }
    }

    public class mdlOSAData
    {
        [DataMember]
        public string ID { get; set; }

        [DataMember]
        public string Week { get; set; }

        [DataMember]
        public string Percent { get; set; }

        [DataMember]
        public string Link { get; set; }

        [DataMember]
        public string CustomerName { get; set; }
    }

    public class mdlWeek
    {
        [DataMember]
        public string Week { get; set; }
    }

    public class mdlOSAStock
    {
        [DataMember]
        public string ProductID { get; set; }

        [DataMember]
        public string ProductName { get; set; }

        [DataMember]
        public string Stock { get; set; }

        
    }

    public class mdlOSACustomer
    {
        [DataMember]
        public string CustomerID { get; set; }

        [DataMember]
        public string CustomerName { get; set; }

        [DataMember]
        public string Link { get; set; }
    }

    public class mdlOSAExcel
    {
        [DataMember]
        public string ID { get; set; }

        [DataMember]
        public string Week { get; set; }

        [DataMember]
        public string Status_zero { get; set; }

        [DataMember]
        public string Status_zeroNone { get; set; }

        [DataMember]
        public string Percent { get; set; }
    }
    
    public class mdlOSAStockExcel
    {
        [DataMember]
        public string ProductID { get; set; }

        [DataMember]
        public string ProductName { get; set; }

        [DataMember]
        public string TotalSisaStock { get; set; }

        [DataMember]
        public string VisitWeek { get; set; }
    }

    public class mdlOSAAccountCustomer
    {
        [DataMember]
        public string ID { get; set; }

        [DataMember]
        public string Week { get; set; }

        [DataMember]
        public string Percent { get; set; }

        [DataMember]
        public string Link { get; set; }

        [DataMember]
        public string ListingAktif { get; set; }

        [DataMember]
        public string Listing { get; set; }
    }

    public class mdlGraphData
    {
        [DataMember]
        public string element { get; set; }

        [DataMember]
        public string data { get; set; }

        [DataMember]
        public string yKey { get; set; }

        [DataMember]
        public string labels { get; set; }
    }

}
