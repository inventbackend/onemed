﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true"
    CodeBehind="UserManagement.aspx.cs" Inherits="TransportService.pages.UserManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Pengaturan Akun</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Form Pengaturan Akun
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="datagrid" style="width: 100%">
                                <table style="width: 100%">
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <br />
                                                <asp:ScriptManager ID="ScriptManager1" runat="server">
                                                </asp:ScriptManager>
                                                <asp:Label ID="lblSearch" runat="server" Text=" Pencarian : "></asp:Label>
                                                <asp:TextBox ID="txtSearch" runat="server" Style="margin-left: 9px; margin-top: 0px"
                                                    Width="200px" Class="tb5" placeholder="Berdasarkan ID Pengguna"></asp:TextBox>
                                                <asp:Button ID="btnSearch" runat="server" Text="Cari" Style="margin-left: 9px;
                                                    margin-top: 0px" class="btn btn-sm btn-default" OnClick="btnSearch_Click"></asp:Button>
                                                <asp:Button ID="btnNew" runat="server" Text="Tambah" class="btn btn-sm btn-primary" OnClick="btnNew_Click" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <asp:Panel ID="Panel1" runat="server" ScrollBars="Horizontal" Width="100%">
                                    <div class="datagrid" ">
                                        <table style="width: 100%" >
                                            <thead>
                                                <tr>
                                                    <th>
                                                        ID Pengguna
                                                    </th>
                                                    <%-- <th>
                                                    Username
                                                </th>--%>
                                                    <th>
                                                        Password
                                                    </th>
                                                    <th>
                                                        ID Cabang
                                                    </th>
                                                    <th>
                                                        Nama Pengguna
                                                    </th>
                                                    <th>
                                                        ID Role
                                                    </th>
                                                    <th>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="RptUserlist" runat="server" OnItemCommand="RptUserlist_ItemCommand">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <asp:LinkButton ID="lnkUserID" runat="server" Enabled='<%# Eval("Role") %>' Text='<%# Eval("UserID") %>'
                                                                    CommandName="Link"></asp:LinkButton>
                                                            </td>
                                                            <%--<td>
                                                            <%# Eval("UserID")%>
                                                        </td>--%>
                                                            <td>
                                                                <%# Eval("Password") %>
                                                            </td>
                                                            <td>
                                                                <%# Eval("BranchID").ToString().Replace("|",",")%>
                                                            </td>
                                                            <td>
                                                                <%# Eval("EmployeeName") %>
                                                            </td>
                                                            <td>
                                                                <%# Eval("RoleID") %>
                                                            </td>
                                                            <td>
                                                                <asp:LinkButton ID="lnkDelete" Text="Hapus" Visible='<%# Eval("Role") %>' OnClientClick="return confirm('Apakah anda yakin ingin menghapus data ini ?');"
                                                                    runat="server" CommandName="Delete" />
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="6">
                                                        <div id="paging">
                                                            <ul>
                                                                <li>
                                                                    <asp:LinkButton ID="lnkPrevious" runat="server" OnClick="lnkPrevious_Click"><span>Previous</span></asp:LinkButton></li>
                                                                <asp:Repeater ID="rptPaging" runat="server" OnItemCommand="rptPaging_ItemCommand">
                                                                    <ItemTemplate>
                                                                        <li>
                                                                            <asp:LinkButton ID="btnPage" CommandName="Page" CommandArgument="<%# Container.DataItem %>"
                                                                                runat="server"><span><%# Container.DataItem %></span></asp:LinkButton>
                                                                        </li>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                                <li>
                                                                    <asp:LinkButton ID="lnkNext" runat="server" OnClick="lnkNext_Click"><span>Next</span></asp:LinkButton></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:Panel ID="PanelFormUser" runat="server" Width="100%">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Form Manajemen Pengguna
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="col-lg-6">
                                    <asp:Label ID="lblUserID" runat="server" Text="ID Pengguna"></asp:Label><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                                        ControlToValidate="txtUserID" ValidationGroup="valUser" ForeColor="#FF3300" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    <asp:TextBox ID="txtUserID" runat="server" class="form-control" TextMode="Search"></asp:TextBox>
                                    
                                </div>
                                <div class="col-lg-6">
                                    <asp:Label ID="lblRoleID" runat="server" Text="ID Role"></asp:Label><br />
                                    <asp:DropDownList ID="ddlRoleID" runat="server" class="form-control">
                                    </asp:DropDownList>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtRoleID" ValidationGroup="valUser" ForeColor="#FF3300"
                                                SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                </div>
                                <div class="col-lg-6" style="display: none;">
                                    <asp:Label ID="lblUsername" runat="server" Text="Username"></asp:Label><br />
                                    <asp:TextBox ID="txtUsername" runat="server" Width="400px" CssClass="tb5"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtUsername" ValidationGroup="valUser" ForeColor="#FF3300" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                </div>
                                <div class="col-lg-12">
                                    <br />
                                </div>

                                <div class="col-lg-6">
                                    <asp:Label ID="Label1" runat="server" Text="ID Cabang"></asp:Label><br />
                                    <asp:ListBox ID="ddlBranchID" runat="server" SelectionMode="Multiple"  data-placeholder="BranchID" CssClass="form-control select2"></asp:ListBox>

                                    
                                   <%-- <asp:TextBox ID="txtBranchID" runat="server" Width="150px" CssClass="tb5" TextMode="Search"
                                        Visible="false"></asp:TextBox>
                                    <asp:Button ID="btnShowBranch" runat="server" Text="Show Branch" Style="margin-left: 0px;
                                        margin-top: 0px" OnClick="btnShowBranch_Click" CssClass="btn btn-sm"></asp:Button>
                                    <asp:Label ID="lblValblBranch" runat="server" Style="color: Red;" Visible="false"></asp:Label>
                                   
                                    <asp:TextBox ID="txtSearchBranch" runat="server" Style="margin-left: 9px; margin-top: 0px"
                                        Width="150px" CssClass="tb5" placeholder="Branch Name.." TextMode="Search" Visible="false"></asp:TextBox>
                                    <asp:Button ID="btnSearchBranch" runat="server" Text="Search Branch Name" Style="margin-left: 9px;
                                        margin-top: 0px" CssClass="btn btn-sm" OnClick="btnSearchBranch_Click" Visible="false">
                                    </asp:Button>
                                    <asp:Button ID="btnCancelBranch" runat="server" Text="Cancel" Style="margin-left: 2px;
                                        margin-top: 0px;" OnClick="btnCancelBranch_Click" CssClass="btn btn-sm"></asp:Button>
                                    <asp:CheckBox ID="CBAllAccess" runat="server" Text=" All Access" OnCheckedChanged="CBAllAccess_CheckedChanged"
                                        AutoPostBack="True" EnableViewState="True"></asp:CheckBox>--%>
                                </div>
                                <div class="col-md-6">
                                    <asp:Label ID="lblEmployee" runat="server" Text="ID Pegawai"></asp:Label><br />           
                                    <asp:ListBox ID="ddlEmployeeID" runat="server"  data-placeholder="EmployeeID" CssClass="form-control select2"></asp:ListBox>

                                    <%--<asp:Panel ID="PanelEmployee" runat="server" Width="70%" Height="200px" ScrollBars="Vertical"
                                        CssClass="datagrid">
                                        <asp:RadioButtonList ID="rblEmployeeID" runat="server">
                                        </asp:RadioButtonList>
                                    </asp:Panel>--%>
                                </div>
                                <%--<div class="col-md-6">
                                    <asp:Panel ID="PanelBranch" runat="server" Width="70%" Height="200px" ScrollBars="Vertical"
                                        CssClass="datagrid">
                                        <asp:CheckBoxList ID="blBranch" runat="server">
                                        </asp:CheckBoxList>
                                    </asp:Panel>
                                </div>--%>

                                <div class="col-lg-12">
                                <br />
                                </div>
                                <div class="col-lg-6">
                                    <asp:Label ID="lblPassword" runat="server" Text="Password"></asp:Label><br />
                                    <asp:TextBox ID="txtPassword" runat="server" class="form-control" TextMode="Password"></asp:TextBox>
                                    <asp:Label ID="lblvalPassword" runat="server" Text="* Password Tidak Sama" ForeColor="Red"
                                        Visible="False"></asp:Label>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtPassword" ValidationGroup="valUser" ForeColor="#FF3300" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                </div>
                                <div class="col-lg-6">
                                    <asp:Label ID="lblRePassword" runat="server" Text="Re-Password"></asp:Label><br />
                                    <asp:TextBox ID="txtRePassword" runat="server" class="form-control" TextMode="Password"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtRePassword" ValidationGroup="valUser" ForeColor="#FF3300" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                </div>
                                <div class="col-lg-12">
                                <br />
                                </div>

                                <div class="col-lg-12">
                                    <asp:Button ID="btnUpdate" runat="server" Text="Ubah" class="btn btn-sm btn-warning" OnClick="btnUpdate_Click"
                                        ValidationGroup="valUser" />
                                    <asp:Button ID="btnInsert" runat="server" Text="Simpan" class="btn btn-sm btn-success" OnClick="btnInsert_Click"
                                        ValidationGroup="valUser" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Batal" class="btn btn-sm btn-default" OnClick="btnCancel_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
<!-- Select2 -->
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2({});
        });
    </script>
    <!-- /Select2 -->
</asp:Content>
