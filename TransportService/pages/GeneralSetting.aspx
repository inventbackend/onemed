﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true" CodeBehind="GeneralSetting.aspx.cs" Inherits="TransportService.pages.GeneralSetting" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">
    function validateQty(event) {
        var key = window.event ? event.keyCode : event.which;
        if (event.keyCode == 8 || event.keyCode == 46
 || event.keyCode == 37 || event.keyCode == 39) {
            return true;
        }
        else if (key < 48 || key > 57) {
            return false;
        }
        else return true;
    };

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <div class ="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Pengaturan Umum</h1>
        </div>
    </div>
    <div class= "row">
        <div class= "col-lg-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2></h2>
                    <div class="clearfix">
                    </div>
                </div>
                
                <div class="x_content">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <fieldset>
                                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                                    </asp:ScriptManager>
                                    <p>
                                    <asp:Label ID="lblBranchID" runat="server" Text="ID Cabang" Width="100px" Font-Bold="true" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px; text-align:center;"></asp:Label>
                                        <asp:Label ID="Label2" runat="server" Text=":" Width="10px" font-Bold="true" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                        <asp:DropDownList ID="ddlBranchID" AutoPostBack="true" OnSelectedIndexChanged="OnSelectedIndexChanged" runat="server">
                                        </asp:DropDownList>
                                        

                                       

                                        </p>
                                       
                                       
                                        <br />
                                        <p style="height:20px;">
                                            <asp:Label ID="lblIdleRadius" runat="server" Text="Radius Idle" Width="100px" Font-Bold="true" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px; text-align:center;"></asp:Label>
                                            <asp:Label ID="Label3" runat="server" Text=":" Width="10px" font-Bold="true" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                            <input id="txtIdleRadius" style="width:150px;" class="form-control col-md-3 col-xs-12" onkeypress='return validateQty(event);' type = "text" runat="server"/>
                                            <asp:Label ID="descRadius" runat="server" Text="(Meter)" Width="10px" font-Bold="false" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                        </p>
                                        <br />
                                        <p style="height:20px;">
                                            <asp:Label ID="lblIdleTime" runat="server" Text="Waktu Idle" Width="100px" Font-Bold="true" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px; text-align:center;"></asp:Label>
                                            <asp:Label ID="Label4" runat="server" Text=":" Width="10px" font-Bold="true" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                            <input id="txtIdleTime" style="width:150px;" class="form-control col-md-3 col-xs-12" onkeypress='return validateQty(event);' type = "text" runat="server"/>
                                            <asp:Label ID="descTime" runat="server" Text="(Menit)" Width="10px" font-Bold="false" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                        </p>
                                        <br />
                                        <p style="height:20px;margin-left:30px;">
                                            <asp:Button ID="btnSave" CssClass="btn btn-primary" runat="server" Text="Simpan" 
                                                onclick="btnSave_Click" />
                                        </p>
                                        
                                </fieldset>
                                
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div id="divMap" style="width:100%; height:500px;"></div>
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
</asp:Content>
