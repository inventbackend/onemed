﻿<%@ Page Title="Customer Coordinate" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true" CodeBehind="CustCoordinate.aspx.cs" Inherits="TransportService.pages.CustCoordinate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="page-title">
        <div class="title_left">
            <h3>
                Form Customer Coordinate</h3>
        </div>

    </div>
    <div class="clearfix">
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>
                    Filter By</h2>
                
                <div class="clearfix">
                </div>
            </div>
            <div class="x_content">
                <br />
                <div class="form-group">
                    <asp:ScriptManager ID="ScriptManager2" runat="server">
                    </asp:ScriptManager>
                    <p>
                    <asp:Label ID="lblBranchID" runat="server" Text="Branch ID" Width="100px" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px; margin-left:-10px"></asp:Label>
                    <asp:Label ID="Label2" runat="server" Text=":" Width="10px" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px; margin-left:5px"></asp:Label>

                        <asp:DropDownList ID="ddlSearchBranchID" runat="server" class="form-control" Style="margin-left:0px; width:150px">
                        </asp:DropDownList>
                    </p>
                    <asp:Button ID="btnShow" runat="server" Text="Show Coordinate" class="btn btn-sm btn-embossed btn-primary"
                        Font-Size="14px" OnClick="btnShow_Click" ValidationGroup="valShowReport" />
                </div>
            </div>
        </div>
    </div>
    <%-- SHOW MAP--%>
    <div class="col-md-12 col-xs-12">
                <asp:Label ID="lblAlert" runat="server" Text="" Style="text-align: center; color: #FF0000;
                    font-weight: 700"></asp:Label>
                <div id="dvMap" style="width: 100%; height: 640px">
          
           
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">

    <link href="../js/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4a8ijWorZUkJKOtzheGKtV1RlyfDYW24&callback=initMap"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <%-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>--%>
    <script src="../js/jquery.blockUI.js" type="text/javascript"></script>
    <script src="../js/FormatNumber.js" type="text/javascript"></script>
    <script src="../js/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
    <script src="../js/jquery.blockUI.js" type="text/javascript"></script>
    <%-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry,places"></script>--%>
    <!-- Support Jquery -->
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script src="../js/FormatNumber.js" type="text/javascript"></script>
    <script src="../js/common.js" type="text/javascript"></script>
    <script src="../js/MapDefault.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/GoogleMap.js"></script>
    <script src="../js/mapwithmarker.js" type="text/javascript"></script>
    <style type="text/css">
        .labels
        {
            color: black;
            background-color: #FF8075;
            font-family: Arial;
            font-size: 11px;
            font-weight: bold;
            text-align: center;
            width: 25px;
        }
    </style>
    <script type="text/javascript">
        var branchCoordinate = [
        <asp:Repeater ID="rptBranchCoordinate" runat="server">
        <ItemTemplate>
                    {
                    "lat": '<%# Eval("Latitude") %>',
                    "lng": '<%# Eval("Longitude") %>'
                }
        </ItemTemplate>
        <SeparatorTemplate>
            ,
        </SeparatorTemplate>
        </asp:Repeater>
        ];
    </script>
    <script type="text/javascript">
        function loadMap() {

            //Create Map
            var objGoogleMapOption = {
                center: new google.maps.LatLng(branchCoordinate[0].lat, branchCoordinate[0].lng),
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            objMap = new google.maps.Map(document.getElementById("dvMap"), objGoogleMapOption);

        }

        $(function () {
            loadMap();
        });
    </script>
    <script type="text/javascript">
        var customerMarkers = [
        <asp:Repeater ID="rptCustomerMarkers" runat="server">
        <ItemTemplate>
                    {
                    "title": '<%# Eval("CustomerName") %>',
                    "lat": '<%# Eval("Latitude") %>',
                    "lng": '<%# Eval("Longitude") %>',
                    "description": '<%# Eval("CustomerName") %>',
                    "address": '<%# Eval("CustomerAddress") %>',
                    "phone": '<%# Eval("Phone") %>',
                    "pic": '<%# Eval("PIC") %>',
                    "CustomerID" : '<%# Eval("CustomerID") %>'
                }
        </ItemTemplate>
        <SeparatorTemplate>
            ,
        </SeparatorTemplate>
        </asp:Repeater>
        ];

        var warehouseMarkers = [
        <asp:Repeater ID="rptWarehouseMarkers" runat="server">
        <ItemTemplate>
                    {
                    "title": '<%# Eval("WarehouseName") %>',
                    "lat": '<%# Eval("Latitude") %>',
                    "lng": '<%# Eval("Longitude") %>',
                    "description": '<%# Eval("WarehouseName") %>',
                    "address": '<%# Eval("WarehouseAddress") %>',
                    "phone": '<%# Eval("Phone") %>',
                    "pic": '<%# Eval("Pic") %>',
                    "CustomerID" : '<%# Eval("CustomerID") %>',
                    "WarehouseID" : '<%# Eval("WarehouseID") %>'
                }
        </ItemTemplate>
        <SeparatorTemplate>
            ,
        </SeparatorTemplate>
        </asp:Repeater>
        ];
    </script>
    <script type="text/javascript">
        window.onload = function () {
            var mapOptions = {
                center: new google.maps.LatLng(customerMarkers[0].lat, customerMarkers[0].lng),
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var infowindow = new google.maps.InfoWindow();
            var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);

            //CustomerCoordinate
            for (i = 0; i < customerMarkers.length; i++) {
                var dataCustomer = customerMarkers[i];
                var myLatlng = new google.maps.LatLng(dataCustomer.lat, dataCustomer.lng);
                var contentString = '<div id="content">' +
                                '<div id="siteNotice">' +
                                '</div>' +
                                '<h2 id="firstHeading" class="firstHeading">' + dataCustomer.description + ' - (' + dataCustomer.CustomerID + ')' + '</h2>' +
                                '<div id="bodyContent">' +
                                '<p><b>Address    : </b>' + dataCustomer.address + '</p>' +
                                '<p><b>Phone : </b>' + dataCustomer.phone + '</p>' +
                                '<p><b>PIC : </b>' + dataCustomer.pic + '</p>' +
                                '</div>' +
                                '</div>';

                var markerCustomer = new MarkerWithLabel({
                    position: myLatlng,
                    map: map,
                    title: dataCustomer.title + " - (" + dataCustomer.CustomerID + ")",
//                    labelContent: i,
                    labelAnchor: new google.maps.Point(15, 65),
                    labelClass: "labels", // the CSS class for the label
                    labelInBackground: false,
                    icon: "../Images/Business-Shop-icon.png"
                });

                google.maps.event.addListener(markerCustomer, 'click', (function (markerCustomer, contentString, infowindow) {
                    return function () {
                        infowindow.setContent(contentString);
                        infowindow.open(map, markerCustomer);
                    };
                })(markerCustomer, contentString, infowindow));
            }

        //WarehouseCoordinate
        for (j = 0; j < warehouseMarkers.length; j++) {
                var dataWarehouse = warehouseMarkers[j];
                var myLatlngWarehouse = new google.maps.LatLng(dataWarehouse.lat, dataWarehouse.lng);
                var contentStringWarehouse = '<div id="content">' +
                                '<div id="siteNotice">' +
                                '</div>' +
                                '<h2 id="firstHeading" class="firstHeading">' + dataWarehouse.description + ' - (' + dataWarehouse.WarehouseID + ')' + '</h2>' +
                                '<div id="bodyContent">' +
                                '<p><b>Address    : </b>' + dataWarehouse.address + '</p>' +
                                '<p><b>Phone : </b>' + dataWarehouse.phone + '</p>' +
                                '<p><b>PIC : </b>' + dataWarehouse.pic + '</p>' +
                                '<p><b>Customer : </b>' + dataWarehouse.CustomerID + '</p>' +
                                '</div>' +
                                '</div>';

                var markerWarehouse = new MarkerWithLabel({
                    position: myLatlngWarehouse,
                    map: map,
                    title: dataWarehouse.title + " - (" + dataWarehouse.WarehouseID + ")",
//                    labelContent: j,
                    labelAnchor: new google.maps.Point(15, 65),
                    labelClass: "labels", // the CSS class for the label
                    labelInBackground: false,
                    icon: "../Images/City-Warehouse-icon.png"
                });

                google.maps.event.addListener(markerWarehouse, 'click', (function (markerWarehouse, contentStringWarehouse, infowindow) {
                    return function () {
                        infowindow.setContent(contentStringWarehouse);
                        infowindow.open(map, markerWarehouse);
                    };
                })(markerWarehouse, contentStringWarehouse, infowindow));
            }
        }
    </script>

</asp:Content>
