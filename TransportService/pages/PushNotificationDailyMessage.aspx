﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true" CodeBehind="PushNotificationDailyMessage.aspx.cs" Inherits="TransportService.pages.PushNotificationDailyMessage" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class ="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Send Data</h1>
        </div>
    </div>
    <div class= "row">
        <div class= "col-lg-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>List Employee</h2>
                    <div class="clearfix">
                    </div>
                </div>
                
                <div class="x_content">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <fieldset>
                                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                                    </asp:ScriptManager>
                                    <p>
                                     
                                        <asp:DropDownList CssClass="form-control" ID="ddlType" runat="server">
                                        </asp:DropDownList>
                                        
                                    </p>
                                    <br />
                                    <p>
                                    <asp:Label ID="lblBranchD" runat="server" Text="Branch ID" Width="100px" Font-Bold="true" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                        <asp:Label ID="Label2" runat="server" Text=":" Width="10px" font-Bold="true" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                        <%--<asp:TextBox ID="txtBranchID" runat="server" Width="150px" CssClass="form-control col-md-3 col-xs-12"></asp:TextBox>
                                        <asp:AutoCompleteExtender ServiceMethod="SearchBranchID" MinimumPrefixLength="1"  
                                            CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtBranchID"  
                                            ID="AutoCompleteExtender1" runat="server" FirstRowSelected="false">  
                                        </asp:AutoCompleteExtender> 

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtBranchID" ValidationGroup="valShowReport" ForeColor="#FF3300"
                                                SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                                <asp:DropDownList ID="ddlSearchBranchID" runat="server" class="form-control" Style="margin-left:0px; width:150px">
                                                </asp:DropDownList>
                                        </p>
                                        <p>
                                             <asp:Label ID="lblEmployeeID" runat="server" Text="Employee ID" Width="100px" Font-Bold="true" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                                    <asp:Label ID="Label4" runat="server" Text=":" Width="10px" Font-Bold="true" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                        <asp:TextBox ID="txtEmployeeID" runat="server" 
                                                        Width="150px" CssClass="form-control col-md-3 col-xs-12" PlaceHolder="Search Employee"></asp:TextBox>

                                            <asp:Button ID="btnShowEmployee" runat="server" CssClass="btn btn-success " Text="Show Employee" onclick="btnShowEmployee_Click" style="margin-left:10px"/>
                                            <asp:Label ID="lblValblEmployee" runat="server" style="color:Red;" visible="false"></asp:Label> <%-- 002 fernandes --%>
                                            <asp:Panel ID="Panel1" CssClass="form-control" Height="200px" Width="400px" runat="server" ScrollBars="Vertical">
                                            <asp:CheckBoxList ID="blEmployee" runat="server">
                                            </asp:CheckBoxList>
                                            </asp:Panel>
                                            <asp:Button ID="btnSelectAll" runat="server" CssClass="btn btn-success " Text="SELECT ALL" onclick="btnSelectAll_Click" />
                                            <asp:Button ID="btnUnSelectAll" runat="server" CssClass="btn btn-success " Text="UNSELECT ALL" onclick="btnUnSelectAll_Click" />
                                        
                                        </p>

                                        <br />
                                    <asp:Button ID="btnSendNotification" CssClass="btn btn-primary" runat="server" 
                                        Text="Send" onclick="btnSendNotification_Click" />

                                </fieldset>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
</asp:Content>
