﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true" CodeBehind="ReportOSA_Account_Customer.aspx.cs" Inherits="TransportService.pages.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <div class="page-title">
        <div class="title_left">
            <h3>
                OSA Customer
            </h3>
        </div>
    </div>
    <div class="clearfix">
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <br />
                

                   <div class="col-md-12">
                            <input action="action" type="button" class="btn btn-sm btn-embossed btn-primary" value="Back"  onclick="window.history.go(-1);" />
                    </div>
                    <br />
                    <div class="datagrid" style="width: 1000px">
                        <table style="width: 100%; margin-bottom: 0px;">
                            <thead>
                                <tr>
                                    <th>
                                        Customer
                                    </th>
                                     <th>
                                        Listing Aktif
                                    </th>
                                     <th>
                                        Listing
                                    </th>
                                    <asp:Repeater ID="RptWeeks" runat="server">
                                        <ItemTemplate>
                                            <th>
                                                <asp:Label ID="lWeek" runat="server" Text='<%# Eval("Week") %>'></asp:Label>
                                            </th>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <asp:Repeater ID="RptOSACustomerlist" runat="server" OnItemDataBound="ItemBound" 
                                        onitemcommand="RptOSACustomerlist_ItemCommand">
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <%# Eval("CustomerID") %> - 
                                                    <asp:Label ID="CustomerName" runat="server" Text='<%# Eval("CustomerName") %>'></asp:Label>
                                                </td>
                                                <%--Repeater for child data percentage--%>
                                                <asp:Repeater ID="RptChildPercentlist" runat="server">
                                                    <ItemTemplate>
                                                     <td>
                                                        
                                                            <asp:Label ID="lblListingAktif" runat="server" Text='<%# Eval("ListingAktif") %>'></asp:Label>
                                                        </td>
                                                         <td>
                                                       
                                                            <%--<asp:Label ID="lblListing" runat="server" Text='<%# Eval("Listing") %>'></asp:Label>--%>
                                                            <a href='<%# Eval("Link") %>'> <%# Eval("Listing") %> </a>
                                                        </td>
                                                        <td>
                                                        <%--<a href='<%# Eval("Link") %>'> <%# Eval("Percent") %> </a>--%>
                                                            <asp:Label ID="Percent" runat="server" Text='<%# Eval("Percent") %>'></asp:Label>
                                                        </td>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tr>
                            </tbody>
                            <tbody>
                                <tr>
                                <td>
                                <b>Grand Total</b>
                                </td>
                                    <asp:Repeater ID="RptChildGrandTotal" runat="server">
                                        <ItemTemplate>
                                        <td></td>
                                        <td></td>
                                            <td>
                                                <b><asp:Label ID="GrandTotal" runat="server" Text='<%# Eval("Percent") %>'></asp:Label></b>
                                            </td>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tr>
                            </tbody>
                            <%--<tfoot>
                                    <tr>
                                        <td colspan="8">
                                            <div id="paging">
                                                <ul>
                                                    <li>
                                                        <asp:LinkButton ID="lnkPrevious" runat="server" OnClick="lnkPrevious_Click"><span>Previous</span></asp:LinkButton></li>
                                                    <asp:Repeater ID="rptPaging" runat="server" OnItemCommand="rptPaging_ItemCommand">
                                                        <ItemTemplate>
                                                            <li>
                                                                <asp:LinkButton ID="btnPage" CommandName="Page" CommandArgument="<%# Container.DataItem %>"
                                                                    runat="server"><span><%# Container.DataItem %></span></asp:LinkButton>
                                                            </li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                    <li>
                                                        <asp:LinkButton ID="lnkNext" runat="server" OnClick="lnkNext_Click"><span>Next</span></asp:LinkButton></li>
                                                </ul>
                                            </div>
                                         </td>
                                    </tr>
                                </tfoot>--%>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
</asp:Content>
