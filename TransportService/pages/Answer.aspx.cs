﻿/* documentation
 *001 nanda - 13 Okt 2016 
 *002 nanda - 21 Okt 2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using Core.Model;
using System.Collections;

namespace TransportService.pages
{
    public partial class Answers : System.Web.UI.Page
    {
        public Core.Model.User vUser;
        

        public bool getRole()
        {
            Globals.gUserId = "";
            Globals.gRoleId = "";

            //get user accesssrole
            Globals.gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(Globals.gUserId);
            Globals.gRoleId = vUser.RoleID;
            string menuName = "Question";
            var vRole = UserFacade.CheckUserLeverage(Globals.gRoleId, menuName);

            return vRole;
        }

        private void ClearContent()
        {
            BindData();

            txtNo.Text = "";
            txtAnswerText.Text = "";
            txtAnswerID.Value = "";

            btnNew.Visible = true;
            btnUpdate.Visible = true;
   
            cbMandatory.Checked = false;
            PanelFormAnswer.Visible = false;
            PanelFormQuestion.Visible = false;

            
        }

        private void BindData()
        {
            var mdlAnswerList = AnswerFacade.LoadAnswer(Globals.gQuestionID, false, Boolean.Parse(ddlIsactive.SelectedValue));

            RptAnswerlist.DataSource = mdlAnswerList;
            RptAnswerlist.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlAnswerType.DataSource = AnswerTypeFacade.LoadAnswerTypeDDL();
                ddlAnswerType.DataTextField = "AnswerTypeText";
                ddlAnswerType.DataValueField = "AnswerTypeID";
                ddlAnswerType.DataBind();

                string[] controlArray = { "True", "False" };
                ddlIsactive.DataSource = controlArray;
                ddlIsactive.DataBind();

                ddlQuestionCategory.DataSource = QuestionCategoryFacade.LoadQuestionCategoryDDL(true);
                ddlQuestionCategory.DataTextField = "QuestionCategoryText";
                ddlQuestionCategory.DataValueField = "QuestionCategoryID";
                ddlQuestionCategory.DataBind();

                ClearContent();

                var qsQuestionID = Request.QueryString["QuestionID"];
                var qsQuestionText = Request.QueryString["QuestionText"];
                var IsMultiple = Request.QueryString["IsMultiple"];

                if (qsQuestionID != null)
                {
                    Globals.gQuestionID = Request.QueryString["QuestionID"];
                    
                }

                if (IsMultiple != null)
                {
                    Globals.dtIsMultiple = Request.QueryString["IsMultiple"];
                }

                if (qsQuestionText != null)
                {
                    Globals.gQuestionText = Request.QueryString["QuestionText"];
                }

                if (Globals.dtIsMultiple == "False")
                {
                    btnNew.Visible = false;
                    btnUpdate.Visible = false;
                }

                lblQuestionText.Text = Globals.gQuestionText;
                lblQuestionID.Text = Globals.gQuestionID;
                BindData();
            }
        }

        protected void RptAnswerlist_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            var dtSequence = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtSequence")).Text);
            var dtNo = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtNo")).Text);
            Globals.dtAnswerID = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtAnswerID")).Text);
            var dtAnswerText = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtAnswerText")).Text);
            var dtsubQuestion = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtsubQuestion")).Text);
            //var dtisActive = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtisActive")).Text);


            if (e.CommandName == "Update")
            {
                txtAnswerID.Value = Globals.dtAnswerID;
                txtAnswerText.Text = dtAnswerText;
                txtNo.Text = dtNo;
                txtSeq.Text = dtSequence;
                

                btnInsert.Visible = false;
                btnUpdate.Visible = true;
         

               PanelFormAnswer.Visible = true;
            }

            if (e.CommandName == "Delete")
            {
                AnswerFacade.NonActiveAnswer(Globals.dtAnswerID);
                ClearContent();
                BindData();
            }

            if (e.CommandName == "Sub_Question")
            {
                ClearContent();
                PanelFormQuestion.Visible = true;

                if (dtsubQuestion == "True")
                {
                    btnInsertSubQuestion.Visible = false;

                    var lmdlQuestion = QuestionFacade.LoadQuestionbyAnswerID(Globals.dtAnswerID);
                    txtNo2.Text = lmdlQuestion.No;
                    txtQuestionID.Value = lmdlQuestion.QuestionID;
                    txtQuestionText.Text = lmdlQuestion.QuestionText;
                    ddlAnswerType.SelectedValue = lmdlQuestion.AnswerTypeID;
                    ddlQuestionCategory.SelectedValue = lmdlQuestion.QuestionCategoryID;
                    cbMandatory.Checked = lmdlQuestion.Mandatory;

                    btnUpdateSubQuestion.Visible = true;
                    btnGotoSubAnswer.Visible = true;
                   
                }
                else
                {

                    txtQuestionID.Value = QuestionFacade.GenerateQuestionID(true);
                    btnGotoSubAnswer.Visible = false;
                    btnUpdateSubQuestion.Visible = false;
                    btnInsertSubQuestion.Visible = true;
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            var lmdlAnswer = new mdlAnswer();
            lmdlAnswer.AnswerID = txtAnswerID.Value;
            lmdlAnswer.AnswerText = txtAnswerText.Text;
            lmdlAnswer.IsActive = true;
            lmdlAnswer.IsSubQuestion = false;
            lmdlAnswer.No = txtNo.Text;
            lmdlAnswer.QuestionID = lblQuestionID.Text;
            lmdlAnswer.Sequence = Int32.Parse(txtSeq.Text);
            lmdlAnswer.SubQuestion = false;

            String lResult = AnswerFacade.UpdateAnswer(lmdlAnswer);
            ClearContent();
        
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            var lmdlAnswer = new mdlAnswer();
            lmdlAnswer.AnswerID = txtAnswerID.Value;
            lmdlAnswer.AnswerText = txtAnswerText.Text;
            lmdlAnswer.IsActive = true;
            lmdlAnswer.IsSubQuestion = false;
            lmdlAnswer.No = txtNo.Text;
            lmdlAnswer.QuestionID = lblQuestionID.Text;
            lmdlAnswer.Sequence = Int32.Parse(txtSeq.Text);
            lmdlAnswer.SubQuestion = false;

            String lResult = AnswerFacade.InsertAnswer(lmdlAnswer);
            ClearContent();
            
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearContent();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            ClearContent();
            PanelFormAnswer.Visible = true;
            txtAnswerID.Value = AnswerFacade.GenerateAnswerID(false);

            txtQuestionID.Value = QuestionFacade.GenerateQuestionID(true);

            btnInsert.Visible = true;
            btnUpdate.Visible = false;
        }

        protected void btnUpdateSubQuestion_Click(object sender, EventArgs e)
        {
            var lmdlQuestion = new mdlQuestion();
            lmdlQuestion.AnswerID = "";
            lmdlQuestion.AnswerTypeID = ddlAnswerType.SelectedValue;
            lmdlQuestion.IsSubQuestion = false;
            lmdlQuestion.Mandatory = cbMandatory.Checked;
            lmdlQuestion.No = txtNo.Text;
            lmdlQuestion.QuestionCategoryID = ddlQuestionCategory.SelectedValue;
            lmdlQuestion.QuestionID = txtQuestionID.Value;
            lmdlQuestion.QuestionText = txtQuestionText.Text;
            lmdlQuestion.QuestionSetID = Globals.gQuestionSetID;
            lmdlQuestion.Sequence = Int32.Parse(txtSeq2.Text);
            lmdlQuestion.IsActive = true;

            String lResult = QuestionFacade.UpdateQuestion(lmdlQuestion);
            ClearContent();

            String Url = string.Format("SubAnswer.aspx?subQuestionID={0}&subQuestionText={1}", txtQuestionID.Value, txtQuestionText.Text);
            Response.Redirect(Url);
        }

        protected void btnInsertSubQuestion_Click(object sender, EventArgs e)
        {
            var lmdlQuestion = new mdlQuestion();
            lmdlQuestion.AnswerID = Globals.dtAnswerID;
            lmdlQuestion.AnswerTypeID = ddlAnswerType.SelectedValue;
            lmdlQuestion.IsActive = true;
            lmdlQuestion.Mandatory = cbMandatory.Checked;
            lmdlQuestion.No = txtNo2.Text;
            lmdlQuestion.QuestionCategoryID = ddlQuestionCategory.SelectedValue;
            lmdlQuestion.QuestionID = txtQuestionID.Value;
            lmdlQuestion.QuestionSetID = Globals.gQuestionSetID;
            lmdlQuestion.QuestionText = txtQuestionText.Text;
            lmdlQuestion.Sequence = Int32.Parse(txtSeq2.Text);
            lmdlQuestion.IsSubQuestion = true;

            String lResult = "";
            var lCheck = AnswerTypeFacade.CheckAnswer(ddlAnswerType.SelectedValue);
            if (lCheck == true)
            {
                lResult = QuestionFacade.InsertSubQuestion(lmdlQuestion, lblQuestionID.Text);
            }
            else
            {
                var lmdlAnswer = new mdlAnswer();
                lmdlAnswer.AnswerID = ""; //generate by system
                lmdlAnswer.AnswerText = ddlAnswerType.SelectedItem.Text.Trim().Split('-')[2];
                lmdlAnswer.IsActive = true;
                lmdlAnswer.IsSubQuestion = true;
                lmdlAnswer.No = "1";
                lmdlAnswer.QuestionID = txtQuestionID.Value;
                lmdlAnswer.Sequence = Int32.Parse(txtSeq2.Text);
                lmdlAnswer.SubQuestion = true;

                lResult = QuestionFacade.InsertQuestionNotMultiple(lmdlQuestion, lmdlAnswer, true, lblQuestionID.Text);
            }
            ClearContent();

            String Url = string.Format("SubAnswer.aspx?subQuestionID={0}&subQuestionText={1}", txtQuestionID.Value, txtQuestionText.Text);
            Response.Redirect(Url);
        }

        protected void btnGotoSubAnswer_Click(object sender, EventArgs e)
        {
            String Url = string.Format("SubAnswer.aspx?subQuestionID={0}&subQuestionText={1}", txtQuestionID.Value, txtQuestionText.Text);
            Response.Redirect(Url);
        }

        protected void btnNonActive_Click(object sender, EventArgs e)
        {
            String lResult = QuestionFacade.NonActiveQuestion(txtQuestionID.Value);
        }

        protected void ddlIsactive_TextChanged(object sender, EventArgs e)
        {
            BindData();
        }

    }
}