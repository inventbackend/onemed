
var counter = 0;
var directionsDisplay = [];
var directionsService = [];
var map = null;
var infowindow = [];

function initialize() {

    var allLocations = new Array();
    var locations = new Array();

    var bounds = new google.maps.LatLngBounds();
    var dataMarkers;

    // var dataMarkersAll;
    var startMrk;

    // var trac = trackingMrk[1];


    // // console.log(trackingMrk.finalTrackingIdleList.length);

    //CHRISTOPHER
//    for (var i = 0; i < trackingMrk.finalTrackingIdleList.length; i++) {
//        // var listTrackingjson = trackingMrk[i];
//        var listTracking = trackingMrk.finalTrackingIdleList[i];
//        // var listTracking = listTrackingjson.trackingIdleList;
//        for (var t = 0; t < listTracking.trackingIdleList.length; t++) {
//            var dataMarkers = listTracking.trackingIdleList[t];
//            locations.push(new google.maps.LatLng(dataMarkers.latitude, dataMarkers.longitude));
//            bounds.extend(locations[locations.length - 1]);
//        }

//    }

//FERNANDES
    for (var i = 0; i < trackingMrk.mdlIdleTrackingList.length; i++) {
        var dataMarkers = trackingMrk.mdlIdleTrackingList[i];
            locations.push(new google.maps.LatLng(dataMarkers.Latitude, dataMarkers.Longitude));
            bounds.extend(locations[locations.length - 1]);
        }





    var mapOptions = {
        //				center: new google.maps.LatLng(allMrk[0].lat,allMrk[0].lng),
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP

    };

    map = new google.maps.Map(document.getElementById('divMap'), mapOptions);
    map.fitBounds(bounds);
    google.maps.event.addDomListener(window, 'resize', function () {
        google.maps.event.trigger(map, 'resize');
        map.fitBounds(bounds);
    });

    // var trafficLayer =  new google.maps.TrafficLayer();
    // trafficLayer.setMap(map);

    // for(var visit = 0;visit<visitMrk.length;visit++){

    // var dataSrc = visitMrk[visit];
    // var longlat = new google.maps.LatLng(dataSrc.lat, dataSrc.lng); 
    // addMarker(longlat,visit,map,dataSrc.time);

    // } 

    // for (var i = 0; i < trackingMrk.length; i++) {
    // listTracking = trackingMrk[i];
    // for(var t = 0; t < listTracking.trackingIdleList.length; t++){
    // if(t==0){
    // var cityCircle = new google.maps.Circle({
    // strokeColor: '#FF0000',
    // strokeOpacity: 0.8,
    // strokeWeight: 2,
    // fillColor: '#FF0000',
    // fillOpacity: 0.35,
    // map: map,
    // // center: citymap[city].center,
    // radius: 100
    // });
    // }
    // var dataSrc = listTracking.trackingIdleList[t];
    // var longlat = new google.maps.LatLng(dataSrc.latitude, dataSrc.longitude);
    // addMarker(longlat,t,map,dataSrc.time)
    // }

    // }

//    var label = 0; CHRISTOPHER

    //CHRISTOPHER
//    for (var i = 0; i < trackingMrk.finalTrackingIdleList.length; i++) {
//        // var listTrackingjson = trackingMrk[i];
//        var listTracking = trackingMrk.finalTrackingIdleList[i];
//        // var listTracking = listTrackingjson.trackingIdleList;
//        for (var t = 0; t < listTracking.trackingIdleList.length; t++) {
//            var track = listTracking.trackingIdleList[t];

//FERNANDES
    for (var i = 0; i < trackingMrk.mdlIdleTrackingList.length; i++) {
        var track = trackingMrk.mdlIdleTrackingList[i];

        var longlat = new google.maps.LatLng(track.Latitude, track.Longitude);

        //CHRIS
//            if (t == 0 && track.isIdle == "1") {
//                var mrk = addMarkerCenter(longlat, label, map, track.time)
//            } else if (track.isIdle == "1") {
//                var mrk = addMarkerIdle(longlat, label, map, track.time)
//            } else if (track.isIdle == "0") {
//                var mrk = addMarker(longlat, label, map, track.time)
//            }

        //FERNANDES
        var mrk = addMarkerIdle(longlat, map, track);

            var citycircle;
            //            if (t == 0 && listTracking.trackingIdleList.length > 1) CHRIS

            if (trackingMrk.mdlIdleTrackingList.length > 0) {
                citycircle = new google.maps.Circle({
                    center: longlat,
                    strokecolor: '#ff0000',
                    strokeopacity: 0.8,
                    strokeweight: 2,
                    fillcolor: '#ff0000',
                    fillopacity: 0.35,
                    map: map,
                    radius: radiusVal
                });


            }

            // var dataSrc = listTracking.trackingIdleList[t];

//            label++; CHRIS
        }

        //    } //CHRIS

}







//FERNANDES
function addMarkerIdle(position, map, data) {

    var contentString = '<div id="content">' +
								'<div id="siteNotice">' +
								'</div>' +
                                '<div id="bodyContent">' +
                                '<p><b>Start    : </b>' + data.StartIdle + '</p>' +
                                '<p><b>End : </b>' + data.EndIdle + '</p>' +
                                '<p><b>Duration : </b>' + data.Duration + '</p>' +
                                '<p><b>Location : </b>' + data.Location + '</p>' +
                                '</div>' +
								'</div>';

    var marker = new MarkerWithLabel({
        position: position,
        map: map,
        title: data.StartIdle,
        icon: 'img/journey/idle.png',
        labelInBackground: false,
        zIndex: 1000
    });

    var iw = new google.maps.InfoWindow({
        content: contentString
    });

    infowindow.push(iw);

    google.maps.event.addListener(marker, "click", function (e) {

        for (var info = 0; info < infowindow.length; info++) {
            infowindow[info].close();
        }

    iw.open(map, marker); });
}

//CHRIS
//function addVisitMrk(position, label, map, title, data) {
//    var newlabel = label + 1;

//    var contentString = '<div id="content">' +
//								'<div id="siteNotice">' +
//								'</div>' +
//								'<h5 id="secondHeading class="secondHeading">Visit ID : ' + data.visitID + '</h3>' +
//								'<h5 id="firstHeading" class="firstHeading">Customer : ' + data.customerName + '</h1>' +
//								'<h5 id="secondHeading class="secondHeading">Start Time : ' + data.startTime + '</h3>' +
//                                '<h5 id="secondHeading class="secondHeading">Finish Time : ' + data.finishTime + '</h5>' +
//								'<h5 id="secondHeading class="secondHeading">Reason : ' + data.reasonName + '</h5>' +
//								'<h5 id="secondHeading class="secondHeading">In Radius : ' + data.isInRange + '</h5>' +
//								'</div>';
//    var marker = new MarkerWithLabel({
//        position: position,
//        map: map,
//        title: title,
//        labelContent: newlabel.toString(),
//        icon: 'img/journey/visit.png',
//        labelAnchor: new google.maps.Point(8, 30),
//        labelClass: "visit",
//        labelInBackground: false,
//        zIndex: 1000


//    });
//    var iw = new google.maps.InfoWindow({
//        content: contentString
//    });

//    google.maps.event.addListener(marker, "click", function (e) { iw.open(map, marker); });


//}
//function addMarker(position, label, map, title) {
//    var newlabel = label + 1;
//    var marker = new MarkerWithLabel({
//        position: position,
//        map: map,
//        icon: 'img/journey/tracking.png',
//        title: title,
//        labelContent: newlabel.toString(),
//        labelAnchor: new google.maps.Point(8, 40),
//        labelClass: "tracking",
//        labelInBackground: false,
//        zIndex: 100


//    });

//    return marker;
//}

//function addMarkerIdle(position, label, map, title) {
//    var newlabel = label + 1;
//    var marker = new MarkerWithLabel({
//        position: position,
//        map: map,
//        icon: 'img/journey/idle.png',
//        title: title,
//        labelContent: newlabel.toString(),
//        labelAnchor: new google.maps.Point(8, 40),
//        labelClass: "tracking",
//        labelInBackground: false,
//        zIndex: 100


//    });

//    return marker;
//}

//function addMarkerCenter(position, label, map, title) {
//    var newlabel = label + 1;
//    var marker = new MarkerWithLabel({
//        position: position,
//        map: map,
//        icon: 'img/journey/center.png',
//        title: title,
//        labelContent: newlabel.toString(),
//        labelAnchor: new google.maps.Point(8, 40),
//        labelClass: "start",
//        labelInBackground: false,
//        zIndex: 1000


//    });

//    return marker;
//}




  
		   
		   
		  
		   
		  