﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;

namespace TransportService.pages
{
    public partial class SettingKoordinatWarehousePop : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string txtWarehouseId = Session["txtWarehouseId"].ToString();
                string cldF = Session["CldFrom"].ToString();
                string cldT = Session["CldTo"].ToString();
                DateTime cldFrom = Convert.ToDateTime(cldF);
                DateTime cldTo = Convert.ToDateTime(cldT);


                var listWarehouse = new List<Core.Model.Warehouse>();
                var warehouse = WarehouseFacade.GetWarehouseDetail(txtWarehouseId);
                if (warehouse == null)
                {

                }
                else
                {
                    lblNama.Text = warehouse.WarehouseName;
                    lblAlamat.Text = warehouse.WarehouseAddress;
                    listWarehouse.Add(warehouse);

                    rptVisit.DataSource = listWarehouse;
                    rptVisit.DataBind();

                    var listKoordinat = VisitFacade.GetKoordinatKunjunganGudang(cldFrom, cldTo, txtWarehouseId);

                    var koordinatMaster = new Core.Model.mdlKoordinatKunjunganGudang();
                    koordinatMaster.Date = "Master";
                    koordinatMaster.Longitude = warehouse.Longitude;
                    koordinatMaster.Latitude = warehouse.Latitude;

                    listKoordinat.Insert(0, koordinatMaster);

                    dgKunjungan1.DataSource = listKoordinat;
                    dgKunjungan1.DataBind();

                    if (listKoordinat.Count > 1)
                    {
                        rptMarkers.DataSource = VisitFacade.GetKoordinatKunjunganGudang(cldFrom, cldTo, txtWarehouseId);
                        rptMarkers.DataBind();
                    }
                    else
                    {
                        rptMarkers.DataSource = listKoordinat;
                        rptMarkers.DataBind();
                    }
                }

                var WarehouseDetail = WarehouseFacade.GetWarehouseDetail(txtWarehouseId);
                radius.Value = Convert.ToString(WarehouseDetail.Radius);
            }

        }

        protected void dgKunjungan_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "id")
            {
                string txtWarehouseId = Session["txtWarehouseId"].ToString();
                var listWarehouse = new List<Core.Model.Warehouse>();
                var warehouse = WarehouseFacade.GetWarehouseDetail(txtWarehouseId);

                warehouse.Longitude = e.Item.Cells[2].Text;
                warehouse.Latitude = e.Item.Cells[3].Text;
                listWarehouse.Add(warehouse);

                rptVisit.DataSource = listWarehouse;
                rptVisit.DataBind();

                lngbox.Value = e.Item.Cells[2].Text;
                latbox.Value = e.Item.Cells[3].Text;
                TextBox3.Text = e.Item.Cells[1].Text;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string txtWarehouseId = Session["txtWarehouseId"].ToString();
            Core.Manager.WarehouseFacade.UpdateLongLatWarehouse(txtWarehouseId, lngbox.Value, latbox.Value, Convert.ToDecimal(radius.Value));
            string cldF = Session["CldFrom"].ToString();
            string cldT = Session["CldTo"].ToString();
            DateTime cldFrom = Convert.ToDateTime(cldF);
            DateTime cldTo = Convert.ToDateTime(cldT);

            Response.Redirect("SettingKoordinatWarehousePop.aspx");
            var listWarehouse = new List<Core.Model.Warehouse>();
            var warehouse = WarehouseFacade.GetWarehouseDetail(txtWarehouseId);
            if (warehouse == null)
            {

            }
            else
            {
                listWarehouse.Add(warehouse);
                rptVisit.DataSource = listWarehouse;
                rptVisit.DataBind();

                var listKoordinat = VisitFacade.GetKoordinatKunjunganGudang(cldFrom, cldTo, txtWarehouseId);

                var koordinatMaster = new Core.Model.mdlKoordinatKunjunganGudang();
                koordinatMaster.Date = "Master";
                koordinatMaster.Longitude = warehouse.Longitude;
                koordinatMaster.Latitude = warehouse.Latitude;
                listKoordinat.Insert(0, koordinatMaster);

                dgKunjungan1.DataSource = listKoordinat;
                dgKunjungan1.DataBind();


                rptMarkers.DataSource = VisitFacade.GetKoordinatKunjunganGudang(cldFrom, cldTo, txtWarehouseId);
                rptMarkers.DataBind();
            }

        }
    }
}