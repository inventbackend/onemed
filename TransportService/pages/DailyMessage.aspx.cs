﻿/* documentation
 *001 
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;

namespace TransportService.pages
{

    public partial class DailyMessage : System.Web.UI.Page
    {
        //fernandes
        public string gBranchId, gUserId, gRoleId;

        public string getBranch()
        {
            //get user branchid
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gBranchId = vUser.BranchID;

            return gBranchId;
        }

        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                    return Convert.ToInt32(ViewState["PageNumber"]);
                else
                    return 0;
            }
            set
            {
                ViewState["PageNumber"] = value;
            }
        }

        public bool getRole()
        {
            //get user accesssrole
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gRoleId = vUser.RoleID;
            string menuName = "Berita Harian";
            var vRole = UserFacade.CheckUserLeverage(gRoleId, menuName);

            return vRole;
        }

        private void ClearContent()
        {
            txtMsgID.Text = string.Empty;
            txtMsgNm.Text = string.Empty;
            txtMsgDesc.Text = string.Empty;
            txtMsgImg.Text = string.Empty;
            txtMsgImgPath.Text = string.Empty;
            txtDate.Text = string.Empty;
            txtEndDate.Text = string.Empty;
            txtMsgCreator.Text = string.Empty;


            btnCancel.Visible = false;
            btnInsert.Visible = false;
            btnUpdate.Visible = false;
            PanelFormDailyMsg.Visible = false;
            Image1.Visible = false;

        }

        public string lParam { get; set; }

        private void BindData(string keyword, DateTime keywordDate, string branchid)
        {
            var listDailyMsg = new List<Core.Model.mdlDailyMsg>();

            if (getBranch() == "")
            {
                listDailyMsg = Core.Manager.DailyMessageFacade.GetSearch(keyword, keywordDate, branchid);
            }
            else
            {
                listDailyMsg = Core.Manager.DailyMessageFacade.GetSearchbySomeBranch(keyword, keywordDate, branchid);
            }

            var role = getRole();
            foreach (var DailyMsg in listDailyMsg)
            {
                DailyMsg.Role = role;
                btnAdd.Visible = role;
            }

            PagedDataSource pgitems = new PagedDataSource();
            pgitems.DataSource = listDailyMsg;
            pgitems.AllowPaging = true;
            pgitems.PageSize = 10;
            pgitems.CurrentPageIndex = PageNumber;
            //lblPage.Text = pgitems.PageCount.ToString();
            if (pgitems.PageCount > 1)
            {
                lnkNext.Visible = true;
                lnkPrevious.Visible = true;
                rptPaging.Visible = true;

                ArrayList pages = new ArrayList();
                for (int i = 0; i < pgitems.PageCount; i++)
                    pages.Add((i + 1).ToString());
                rptPaging.DataSource = pages;
                rptPaging.DataBind();

                Control control;
                control = rptPaging.Items[PageNumber].FindControl("btnPage");
                if (control != null)
                {
                    ((LinkButton)(rptPaging.Items[PageNumber].FindControl("btnPage"))).Attributes["class"] = "active";
                    ((LinkButton)(rptPaging.Items[PageNumber].FindControl("btnPage"))).Attributes["ForeColor"] = "Black";
                }

                if (PageNumber == 0)
                {
                    lnkPrevious.Enabled = false;
                    lnkNext.Enabled = true;
                }
                else if (PageNumber == pgitems.PageCount - 1)
                {
                    lnkPrevious.Enabled = true;
                    lnkNext.Enabled = false;
                }
                else
                {
                    lnkPrevious.Enabled = true;
                    lnkNext.Enabled = true;
                }
            }
            else
            {
                rptPaging.Visible = false;
                lnkPrevious.Visible = false;
                lnkNext.Visible = false;
            }

            RptDailyMsglist.DataSource = pgitems;
            RptDailyMsglist.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtSearch2.Text = DateTime.Now.ToString("MM/dd/yyyy");
                BindData("", DateTime.Now,getBranch());
                ClearContent();
            }
        }

        protected void rptDailyMsg_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                Control control;
                control = e.Item.FindControl("MsgID");

                if (control != null)
                {
                    string id;
                    id = ((Label)control).Text;

                    DailyMessageFacade.DeleteDailyMessage(id);

                    BindData("", DateTime.Now,getBranch());
                }
            }

            if (e.CommandName == "Update")
            {
                Control control;
                control = e.Item.FindControl("MsgID");
                if (control != null)
                {
                    txtMsgID.Text = ((Label)control).Text;
                    var DailyMsgUpdate = DailyMessageFacade.GetDailyMessageByID(txtMsgID.Text);
                    txtMsgNm.Text = DailyMsgUpdate.MessageName;
                    txtMsgID.ReadOnly = true;
                    txtMsgDesc.Text = DailyMsgUpdate.MessageDesc;
                    Image1.ImageUrl = "data:image/png;base64," + DailyMsgUpdate.MessageImg;  //002
                    Image1.Visible = true;
                    txtMsgImg.Text = DailyMsgUpdate.MessageImg;
                    txtMsgImgPath.Text = DailyMsgUpdate.ImgPath;
                    txtDate.Text = DailyMsgUpdate.Date.ToString("dd MMM yyyy");
                    txtEndDate.Text = DailyMsgUpdate.EndDate.ToString("dd MMM yyyy");
                    txtMsgCreator.Text = DailyMsgUpdate.CreatedBy;

                    btnInsert.Visible = false;
                    btnCancel.Visible = true;
                    btnUpdate.Visible = true;
                    PanelFormDailyMsg.Visible = true;
                }
            }

            if (e.CommandName == "Role")
            {
                Control control;
                control = e.Item.FindControl("MsgID");
                if (control != null)
                {
                    string id;
                    id = ((Label)control).Text;

                    Response.Redirect("DailyMessageDetail.aspx?cid=" + id);
                }
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            txtMsgID.ReadOnly = false;
            txtMsgID.Text = string.Empty;
            txtMsgNm.Text = string.Empty;
            txtMsgDesc.Text = string.Empty;
            txtMsgImg.Text = string.Empty;
            txtMsgImgPath.Text = string.Empty;

            txtDate.Text = string.Empty;
            txtEndDate.Text = string.Empty;
            txtMsgCreator.Text = string.Empty;
            btnCancel.Visible = true;
            btnInsert.Visible = true;
            btnUpdate.Visible = false;
            Image1.Visible = false;
            PanelFormDailyMsg.Visible = true;
        }

        protected void rptPaging_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            PageNumber = Convert.ToInt32(e.CommandArgument) - 1;
            BindData(txtSearch.Text, Convert.ToDateTime(txtSearch2.Text),getBranch());
        }

        protected void lnkPrevious_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber - 1;

            BindData(txtSearch.Text, Convert.ToDateTime(txtSearch2.Text),getBranch());
        }

        protected void lnkNext_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber + 1;

            BindData(txtSearch.Text, Convert.ToDateTime(txtSearch2.Text),getBranch());
        }

        protected void btnSearch2_Click(object sender, EventArgs e)
        {
            lParam = "search";
            PageNumber = 0;
            try
            {
                BindData(txtSearch.Text, Convert.ToDateTime(txtSearch2.Text), getBranch());
            }
            catch
            {
                DateTime txtDate = DateTime.ParseExact(txtSearch2.Text, "MM-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                BindData(txtSearch.Text, txtDate, getBranch());
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string lastdate = DateTime.Now.ToString();
            string lastupdateby = Session["User"].ToString();

            DailyMessageFacade.UpdateDailyMessage(txtMsgID.Text, txtMsgNm.Text, Convert.ToDateTime(txtDate.Text), Convert.ToDateTime(txtEndDate.Text), txtMsgDesc.Text, txtMsgCreator.Text, txtMsgImg.Text, txtMsgImgPath.Text);
            BindData("", DateTime.Now,getBranch());

            ClearContent();
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            var DailyMsg = DailyMessageFacade.GetDailyMessageByID(txtMsgID.Text);
            if (DailyMsg != null)
            {
                Response.Write("<script>alert('Message already Exist');</script>");
                return;
            }

            DailyMessageFacade.InsertDailyMessage(txtMsgID.Text, txtMsgNm.Text, Convert.ToDateTime(txtDate.Text), Convert.ToDateTime(txtEndDate.Text), txtMsgDesc.Text, txtMsgCreator.Text, txtMsgImg.Text, txtMsgImgPath.Text);
            
            List<String> blStrList = new List<string>();
            blStrList.Add("");

            //Insert to dailymessagedetail because there is certain condition
            var Branchlist = new List<Core.Model.mdlBranch>();

            if(getBranch() == "")
            {
                DailyMessageFacade.InsertDailyMessageDetail(txtMsgID.Text, "", blStrList);
            }
            else
            {
                Branchlist = Core.Manager.BranchFacade.LoadSomeBranch(getBranch());

                foreach (var Branch in Branchlist)
                {
                    DailyMessageFacade.InsertDailyMessageDetail(txtMsgID.Text, Branch.BranchID, blStrList);
                }
            }
            //--

            Response.Redirect("DailyMessage.aspx");

            ClearContent();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearContent();
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                string fileExt = System.IO.Path.GetExtension(FileUpload1.FileName);

                if (fileExt == ".jpeg" || fileExt == ".jpg" || fileExt == ".JPG" || fileExt == ".JPEG")
                {
                    //do what you want with this file
                    string filename = FileUpload1.FileName;
                    FileUpload1.PostedFile.SaveAs(Server.MapPath(@"~\Images\DailyMessage\" + filename.Trim()));
                    string path = @"~\Images\DailyMessage\" + filename.Trim();

                    System.IO.Stream fs = FileUpload1.PostedFile.InputStream;
                    System.IO.BinaryReader br = new System.IO.BinaryReader(fs);
                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                    string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                    Image1.ImageUrl = "data:image/png;base64," + base64String;
                    Image1.Visible = true;
                    //txtMsgImg.Text = "data:image/png;base64," + base64String;
                    txtMsgImg.Text = base64String;    
                    txtMsgImgPath.Text = path;

                    Label1.Text = "";
                }
                else
                {
                    Label1.Text = "Only .jpeg files allowed!";
                }
            }
            else
            {
                Label1.Text = "You have not specified a file.";
            }

        }

    }
    
}