﻿<%@ Page Title="Sales Order Report" Language="C#" AutoEventWireup="true" CodeBehind="ReportSalesOrder.aspx.cs"
    MasterPageFile="~/pages/Site.Master" Inherits="TransportService.pages.ReportSalesOrder" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <div class="title_left">
            <h3>
                Form Laporan Penjualan</h3>
        </div>
        <div class="title_right">
        </div>
    </div>

    <div class="clearfix">
    </div>
    
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <br />
                                <div class="form-group">
                                <fieldset>
                                    <asp:ScriptManager ID="ScriptManager2" runat="server">
                                    </asp:ScriptManager>
                                    <p>
                                        <asp:Label ID="lblBranchD" runat="server" Text="ID Cabang" Width="100px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                        <asp:Label ID="Label2" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>

                                        <asp:DropDownList ID="ddlSearchBranchID" runat="server" class="form-control" Style="margin-left:0px; width:150px">
                                        </asp:DropDownList>
                                    </p>

                                    <p>

                                        <asp:Label ID="lblEmployeeID" runat="server" Text="ID Pegawai" Width="100px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                                    <asp:Label ID="Label4" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                        <asp:TextBox ID="txtEmployeeID" runat="server" 
                                                        Width="150px" CssClass="form-control col-md-3 col-xs-12" PlaceHolder="Search Employee"></asp:TextBox>

                                        <asp:Button ID="btnShowEmployee" runat="server" CssClass="btn btn-success" Text="Tampilkan" onclick="btnShowEmployee_Click" style="margin-left:10px"/>
                                        <asp:Label ID="lblValblEmployee" runat="server" style="color:Red;" visible="false"></asp:Label>
                                        <asp:Panel ID="Panel1" CssClass="form-control" Height="200px" Width="400px" runat="server" ScrollBars="Vertical" style="margin-left:10px">
                                        <asp:CheckBoxList ID="blEmployee" runat="server">
                                        </asp:CheckBoxList>
                                        </asp:Panel>
                                        <asp:Button ID="btnSelectAll" runat="server" CssClass="btn btn-success" Text="Pilih Semua" onclick="btnSelectAll_Click" style="margin-left:10px"/>
                                         <asp:Button ID="btnUnSelectAll" runat="server" CssClass="btn btn-success " Text="Batal Pilih Semua" onclick="btnUnSelectAll_Click" style="margin-left:10px"/>
                                    </p>
                                                
                                    <p>
                                        <asp:Label ID="lbl2" runat="server" Text="Tanggal" Width="100px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                        <asp:Label ID="Label3" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                        <asp:TextBox ID="txtdate1" runat="server" 
                                            Width="150px" CssClass="form-control col-md-3 col-xs-12" PlaceHolder="Start Date"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="- Start Date Required -"
                                                ControlToValidate="txtdate1" ValidationGroup="valShowReport" ForeColor="#FF3300"
                                                SetFocusOnError="True"></asp:RequiredFieldValidator>

                                                <script type="text/javascript">
                                                    var picker1 = new Pikaday(
                                                 {
                                                     field: document.getElementById('<%=txtdate1.ClientID%>'),
                                                     firstday: 1,
                                                     minDate: new Date('2000-01-01'),
                                                     maxDate: new Date('2020-12-31'),
                                                     yearRange: [2000, 2020],
                                                     format: 'MM-DD-YYYY'
                                                     //setDefaultDate: true,
                                                     //defaultDate : new Date()
                                                 }
                                                                          );
                                        </script>
                                                
                                        <asp:TextBox ID="txtdate2" runat="server" Width="150px" CssClass="form-control col-md-3 col-xs-12" PlaceHolder="End Date" style="margin-left:10px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="- End Date Required -"
                                                ControlToValidate="txtdate2" ValidationGroup="valShowReport" ForeColor="#FF3300"
                                                SetFocusOnError="True"></asp:RequiredFieldValidator>

                                                <script type="text/javascript">
                                                    var picker1 = new Pikaday(
                                                 {
                                                     field: document.getElementById('<%=txtdate2.ClientID%>'),
                                                     firstday: 1,
                                                     minDate: new Date('2000-01-01'),
                                                     maxDate: new Date('2020-12-31'),
                                                     yearRange: [2000, 2020],
                                                     format: 'MM-DD-YYYY'
                                                     //setDefaultDate: true,
                                                     //defaultDate : new Date()
                                                 }
                                                                          );
                                        </script>
                                        
                                    </p>
                                    <br />
                                    <asp:Button ID="btnShow" runat="server" Text="Tampilkan Laporan" class="btn btn-sm btn-embossed btn-primary"
                                        Font-Size="14px" OnClick="btnShow_Click" ValidationGroup="valShowReport" />
                                        <%--<asp:Button ID="btnShowExcel" runat="server" Text="Show Report Excel" class="btn btn-sm btn-embossed btn-primary"
                                        Font-Size="14px" OnClick="btnShowExcel_Click" ValidationGroup="valShowReport" />--%>
                                        </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
            <%-----------------------------------------------------------// Start Cut Form //--------------------------------------------------------------------------------------%>
            <br />
            <br />
            <div class="col-md-12 col-xs-12">
                <asp:Panel ID="PanelReportSalesOrder" runat="server" Height="500px" Width="100%" CssClass="datagrid">
                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"
                        Width="100%" Height="500px" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana"
                        WaitMessageFont-Size="14pt">
                        <LocalReport ReportPath="Report\ReportSalesOrder.rdlc">
                        </LocalReport>
                    </rsweb:ReportViewer>
                </asp:Panel>
            </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
<script type="text/javascript">
    $(document).ready(function () {
        $("input[type=checkbox]").addClass("flat");
    });
        
    </script>
</asp:Content>
