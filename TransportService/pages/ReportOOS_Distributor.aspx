﻿<%@ Page Title="Report OOS Distributor" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true" CodeBehind="ReportOOS_Distributor.aspx.cs" Inherits="TransportService.pages.ReportOOS_Distributor" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<link href="../vendors/morris.js/morris.css" rel="stylesheet">
<script src="js/journey/jquery-1.11.3.js"></script>
<script src="../vendors/raphael/raphael.min.js"></script>
 <script src="../vendors/morris.js/morris.min.js"></script>
 <!--bar fix-->
    <script>
        var arrChart = [];
    </script>
    <!--bar fix-->
<style>
.tablefixed {
  
  overflow-x: scroll;
 
  overflow-y: visible;
  padding-bottom: 1px;
}
.td {
  padding: 0 40px;
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<asp:ScriptManager ID="ScriptManager2" runat="server">
                                    </asp:ScriptManager>
    <div class="page-title">
         <div class="title_left">
            <h3>
                Form Report OOS</h3>
                <div class="title_right">
                </div>
         </div>
          
    </div>
    <div class="clearfix">
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
        <div class="x_title">
        <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                    </ul>
                    <div class="clearfix"></div>
            
        </div>
            <div class="x_content">
                <br />
                    <div class="form-group col-md-12 col-xs-12 col-lg-12">
                        <div class="col-md-12 col-xs-12 col-lg-12">
                            <div class="row" style="margin-bottom:10px;">
                                <div class="col-xs-12 col-md-6">
                                    
                                    <asp:Label ID="lblWeekFrom" runat="server" Text="From Week" Width="100px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                    <asp:DropDownList ID="ddlWeekFrom" class="form-control" runat="server">
                                        
                                    </asp:DropDownList>
                                    
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <asp:Label ID="lblWeekTo" runat="server" Text="To Week" Width="100px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                    <asp:DropDownList ID="ddlWeekTo" class="form-control" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom:10px;">
                                <div class="col-xs-12 col-md-6">
                                    <asp:Label ID="Label1" runat="server" Text="By" Width="100px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                              
                                    <asp:DropDownList ID="ddlBy" runat="server" class="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlBy_SelectedIndexChanged">
                                        <asp:ListItem Text="AREA" Value="AREA"></asp:ListItem>
                                        <asp:ListItem Text="ACCOUNT" Value="ACCOUNT"></asp:ListItem>
                                    </asp:DropDownList>

                                </div>
                                <div class="col-xs-12 col-md-6">
                                <asp:Label ID="Label2" runat="server" Text="Year" Width="100px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                              
                                    <asp:DropDownList ID="ddlYear" runat="server" class="form-control" >
                                        
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom:10px;">
                                <div class="col-xs-12 col-md-6">
                                    <div class="row" style="margin-bottom:10px;">
                                        <div class="col-xs-12 col-md-6">
                                            <asp:Label ID="lblAreaOrAccount" runat="server" Text="Area" Width="100px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                        </div>
                                       
                                    </div>
                                    <div class="row" style="margin-bottom:10px;">
                                        <div class="col-xs-12 col-md-7">
                                        
                                            <div class="col-xs-12 col-md-12" style="overflow-y:scroll;height:300px;">
                                                <asp:CheckBoxList ID="cblArea" runat="server">
                                                </asp:CheckBoxList>

                                                <asp:CheckBoxList ID="cblAccount" runat="server">
                                                </asp:CheckBoxList>
                                                
                                            </div>
                                            <div class="row" style="margin-bottom:10px;">
                                                <div class = "col-xs-12 col-md-5">
                                                    <asp:Button ID="btnSelectAll" runat="server" CssClass="btn btn-success" Text="SELECT ALL" onclick="btnSelectAll_Click" style="margin-left:10px"/>
                                                
                                                </div>
                                                <div class = "col-xs-12 col-md-6">
                                                    <asp:Button ID="btnUnSelectAll" runat="server" CssClass="btn btn-success " Text="UNSELECT ALL" onclick="btnUnSelectAll_Click" style="margin-left:10px"/>
                                                </div>
                                                
                                            </div>
                                       
                                        </div>
                                        
                                        
                                    </div>
                                    <div class="row">
                                     <div class="col-md-12 col-xs-12" >
                                        <asp:RadioButtonList ID="rblRole" RepeatDirection="Horizontal"  runat="server">
                                            <asp:ListItem Text="All &nbsp &nbsp" Value="" />
                                            <asp:ListItem Text="MD &nbsp &nbsp" Value="R0001" />
                                            <asp:ListItem Text="BP" Value="R0006" />
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                    <div class="row">   
                                        <div class="col-md-12 col-xs-12">
                                            <asp:Button ID="btnShow" runat="server" Text="Show Report" class="btn btn-sm btn-embossed btn-primary"
                                        Font-Size="14px" OnClick="btnShow_Click" />
                                        <asp:Button ID="btnShowExcel" runat="server" Text="Show Report Excel" class="btn btn-sm btn-embossed btn-primary"
                                        Font-Size="14px" OnClick="btnShowExcel_Click" ValidationGroup="valShowReport" />
                                        </div>
                                        


                                    </div>

                                    

                                    <div class="row">
                                        <div class="col-md-12 col-xs-12">
                                            <asp:Panel ID="PanelReportOOS" runat="server" Height="500px" Width="100%" CssClass="datagrid">
                                                <rsweb:ReportViewer ID="reportOOS" runat="server" Font-Names="Verdana" Font-Size="8pt"
                                                    Width="100%" Height="500px" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana"
                                                    WaitMessageFont-Size="14pt">
                                                    <LocalReport ReportPath="Reports\ReportOOS.rdlc">
                                                    </LocalReport>
                                                </rsweb:ReportViewer>
                                            </asp:Panel>
                                        </div>
                                    
                                    </div>
                                    
                                </div>
                                <div class="col-xs-12 col-md-6">
                                <!--Product------------------------------------>
                                    <div class="row" style="margin-bottom:10px;">
                                        <div class="col-xs-12 col-md-6">
                                            <asp:Label ID="Label3" runat="server" Text="Distributor" Width="100px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                        </div>
                                       
                                    </div>
                                     <div class="row" style="margin-bottom:10px;">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="col-xs-12 col-md-12" style="overflow-y:scroll;height:300px;">
                                                <asp:RadioButtonList ID="rblDistributor" runat="server">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                     </div>
                                </div>
                            </div>
                            
                         </div>
                       
                    </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                    <h2><i class="fa fa-bars"></i>  <small>Data</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                    </ul>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab1" class="nav nav-tabs bar_tabs right" role="tablist">
                        <li role="presentation" class=""><a href="#tab_content11" id="home-tabb" role="tab"
                            data-toggle="tab" aria-controls="home" aria-expanded="true">Charts</a> </li>
                        <li role="presentation" class="active"><a href="#tab_content22" role="tab" id="profile-tabb"
                            data-toggle="tab" aria-controls="profile" aria-expanded="false">Tables</a> </li>
                    </ul>
                      <div id="myTabContent2" class="tab-content">
                         <div role="tabpanel" class="tab-pane fade" id="tab_content11" aria-labelledby="home-tab">
                            <div class="row">
                            
                          <asp:Repeater ID="rptBranchChart" runat="server">
                          <ItemTemplate>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2><%# Eval("Area") %></h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                          </li>
                                          
                                        </ul>
                                        <div class="clearfix"></div>
                                      </div>
                                      <div class="x_content">
                                        <div id='<%# Eval("ID") %>' style="width:100%; height:280px;"></div>
                                         <script>
//                                             $(document).ready(function () {
//                                                 Morris.Bar({
//                                                     element: <%# Eval("ID") %> ,
//                                                     data: <%# Eval("json") %>,
//                                                     xkey: 'VisitWeek',
//                                                     ykeys: ['OOS'],
//                                                     labels: ['OOS'],
//                                                     barRatio: 0.4,
//                                                     barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
//                                                     xLabelAngle: 35,
//                                                     hideHover: 'auto',
//                                                     resize: true
//                                                 });
//                                             });
                                                arrChart.push(new Morris.Bar({
                                                     element: <%# Eval("ID") %> ,
                                                     data: <%# Eval("json") %>,
                                                     xkey: 'VisitWeek',
                                                     ykeys: ['OOS'],
                                                     labels: ['OOS'],
                                                     barRatio: 0.4,
                                                     barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                                                     xLabelAngle: 35,
                                                     hideHover: 'auto',
                                                     resize: true,
                                                     redraw:true }));
                                        </script>
                                      </div>
                                </div>
                            </div>
                            </ItemTemplate>
                          </asp:Repeater>
                          </div>
                        </div>
                         <div role="tabpanel" class="tab-pane fade active in" id="tab_content22" aria-labelledby="profile-tab">
                             <div class="row">
                                <div class="col-md-12 datagrid tablefixed" runat="server" id="divTable">
                                            
                                            <table style="width:100%;margin-bottom: 0px;">

                                                <asp:Repeater ID="rptHeader" runat="server">
                                                <HeaderTemplate>
                                                <thead>
                                                    <th>
                                                      
                                                             <asp:Label ID="lblHeader" runat="server" Text='AREA'></asp:Label>
                                                      </th>
                                                </HeaderTemplate>
                                                    
                                                <ItemTemplate>
                                                    <th>
                                                        <asp:Label ID="lWeek" runat="server" Text='<%# Container.DataItem %>'></asp:Label>
                                                    </th>
                                                </ItemTemplate>
                                                    <FooterTemplate>
                                                    
                                                    
                                                    </tr>
                                                    </thead>
                                                    </FooterTemplate>
                                                </asp:Repeater>

                                                <asp:Repeater ID="rptparent" runat="server" OnItemDataBound="ItemBound">
                                                    
                                                     <ItemTemplate>
                                                        <tbody>
                                                        <tr >
                                                        <td  style="position:absolute; left:0;">
                                                            <a href='<%# Eval("Link") %>'><asp:Label ID="lblArea" runat="server" Text='<%# Eval("Area") %>'></asp:Label></a>
                                                        </td>
                                                        <asp:Repeater ID="rptChild" runat="server">
                                                            <ItemTemplate>
                                                                <td>
                                                                    <asp:Label ID="lblData" runat="server" Text='<%# Eval("OOS") %>'></asp:Label>%
                                                                </td>
                                                            </ItemTemplate>
                                                        </asp:Repeater>


                                                        </tr>
                                                        
                                                            
                                                           <%-- <asp:Repeater ID="rptParentAccount" runat="server" OnItemDataBound="AccountItemBound">
                                                                <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                       <a href='<%# Eval("Link") %>'> <asp:Label ID="lblDataParenAccount" runat="server" Text='<%# Eval("AccountID") %>'></asp:Label></a>
                                                                    </td>
                                                                    <asp:Repeater ID="rptChildAccount" runat="server">
                                                                        <ItemTemplate>
                                                                            <td>
                                                                                <asp:Label ID="lblDataChildAccount" runat="server" Text='<%# Eval("OOS") %>'></asp:Label>%
                                                                             </td>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                   </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>--%>
                                                            <%--<tr >
                                                            
                                                                <td style="font-weight:bold">Total</td>
                                                                <asp:Repeater ID="rptTotal" runat="server">
                                                                    <ItemTemplate>
                                                                    <td style="font-weight:bold">
                                                                        <asp:Label ID="lblTotal" runat="server" Text='<%# Container.DataItem %>'></asp:Label>%
                                                                    </td>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </tr>--%>
                                                       
                                                        </tbody>
                                                     </ItemTemplate>
                                                </asp:Repeater>
                                                    <tbody>
                                                        <tr >
                                                            <td style="font-weight:bolder;"><asp:Label ID="lbltot" runat="server" Text='GRAND TOTAL'></asp:Label></td>
                                                            <asp:Repeater ID="rptGrandTotal" runat="server">
                                                                <ItemTemplate>
                                                                    <td style="font-weight:bolder;">
                                                                        <asp:Label ID="lblGrandTotal" runat="server" Text='<%# Container.DataItem %>'></asp:Label>%
                                                                    </td>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </tr>
                                                    </tbody>
                                            
                                            
                                            </table>

                                        </div>
                             </div>
                             <div class="row">
                                        
                                        
                                        

                                        <div class="col-md-12 datagrid tablefixed" runat="server" id="divTableAccount">
                                            
                                            <table style="width:100%;margin-bottom: 0px;">

                                                <asp:Repeater ID="rptHeaderReportAccount" runat="server">
                                                <HeaderTemplate>
                                                <thead>
                                                    <th>
                                                      
                                                             <asp:Label ID="lblHeader" runat="server" Text='ACCOUNT'></asp:Label>
                                                      </th>
                                                      <%--<th>
                                                      
                                                             <asp:Label ID="Label3" runat="server" Text='Product ID'></asp:Label>
                                                      </th>--%>
                                                </HeaderTemplate>
                                                    
                                                <ItemTemplate>
                                                    <th>
                                                        <asp:Label ID="lWeek" runat="server" Text='<%# Container.DataItem %>'></asp:Label>
                                                    </th>
                                                </ItemTemplate>
                                                    <FooterTemplate>
                                                    
                                                    
                                                    </tr>
                                                    </thead>
                                                    </FooterTemplate>
                                                </asp:Repeater>

                                                <asp:Repeater ID="rptParentReportAccount" runat="server" OnItemDataBound="ReportAccountItemBound">
                                                    
                                                     <ItemTemplate>
                                                        <tbody>
                                                        <tr>
                                                        <td  style=" left:0;">
                                                            <a href='<%# Eval("Link") %>'><asp:Label ID="lblAccount" runat="server" Text='<%# Eval("AccountID") %>'></asp:Label></a>
                                                        </td>
                                                        <%--<td  style=" left:0;">
                                                            <asp:Label Visible="false" ID="lblProductID" runat="server" Text='<%# Eval("ProductID") %>'></asp:Label>
                                                            <asp:Label ID="lblProductName" runat="server" Text='<%# Eval("ProductName") %>'></asp:Label>
                                                        </td>--%>
                                                        <asp:Repeater ID="rptChildAccountStock" runat="server">
                                                            <ItemTemplate>
                                                                <td>
                                                                    <asp:Label ID="lblData" runat="server" Text='<%# Eval("OOS") %>'></asp:Label>
                                                                </td>
                                                            </ItemTemplate>
                                                        </asp:Repeater>


                                                        </tr>
                                                        
                                                            
                                                            <%--<asp:Repeater ID="rptParentAccount" runat="server" OnItemDataBound="AccountItemBound">
                                                                <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblDataParenAccount" runat="server" Text='<%# Eval("AccountID") %>'></asp:Label>
                                                                    </td>
                                                                    <asp:Repeater ID="rptChildAccount" runat="server">
                                                                        <ItemTemplate>
                                                                            <td>
                                                                                <asp:Label ID="lblDataChildAccount" runat="server" Text='<%# Eval("OOS") %>'></asp:Label>%
                                                                             </td>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                   </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>--%>
                                                            
                                                       
                                                        </tbody>
                                                     </ItemTemplate>
                                                </asp:Repeater>
                                                <tbody>
                                                        <tr >
                                                            <td style="font-weight:bolder;"><asp:Label ID="Label4" runat="server" Text='GRAND TOTAL'></asp:Label></td>
                                                            
                                                            <asp:Repeater ID="rptAccountGrandTotal" runat="server">
                                                                <ItemTemplate>
                                                                    <td style="font-weight:bolder;">
                                                                        <asp:Label ID="lblGrandTotal" runat="server" Text='<%# Container.DataItem %>'></asp:Label>%
                                                                    </td>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </tr>
                                                    </tbody>
                                            
                                            </table>

                                        </div>

                                    </div>
                         </div>
                      </div>
                </div>
            </div>
        </div>
    </div>
    <!--bar fix-->
    <script type="text/javascript">
        $(document).ready(function () {

            $('.nav-tabs a').on('shown.bs.tab', function (event) {
                for (var i = 0; i < arrChart.length; i++) {
                    arrChart[i].redraw();
                }
            });
        });
    </script>
<!--bar fix-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
</asp:Content>

