﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true" CodeBehind="ReportOOSCustomer.aspx.cs" Inherits="TransportService.pages.ReportOOSCustomer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript">
    function Navigate() {

        window.history.go(-1);
    }
    </script>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <div class="form-group col-md-12 col-xs-12 col-lg-12">
                    <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <%--<asp:Button ID="btnback" runat="server" Text="Back" class="btn btn-sm btn-embossed btn-primary"
                                        Font-Size="14px" OnClientClick="Navigate()" />--%>
                                        <input action="action" type="button" class="btn btn-sm btn-embossed btn-primary" value="Back"  onclick="window.history.go(-1);" />
                                </div>
                            </div>
                    <div class="col-md-12 datagrid tablefixed" runat="server" id="divTableAccount">
                        <table style="width:100%;margin-bottom: 0px;">

                            <thead>
                                <th>Name of Store</th><th>Account</th><th>Channel</th><th>Area</th>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptCustomers" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Eval("CustomerName")%></td> <td><%# Eval("Account") %></td> <td><%# Eval("Channel")%></td> <td><%# Eval("BranchID")%></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
</asp:Content>
