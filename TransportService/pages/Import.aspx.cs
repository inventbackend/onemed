﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;

namespace TransportService.pages
{
    public partial class Import : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void alertSuccess()
        {
            string script = "alert('Import Success');";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", script, true);
        }

        protected void btnUpload_Click(Object sender, EventArgs e)
        {

            string importType = ddlImport.SelectedValue.ToString();
            


            if (fileUpload.HasFile)
            {
                //try
                //{
                    if (importType == "CALLPLAN")
                    {
                        string filename = "Intenary.xlsx";
                        string path = Server.MapPath("~/import/") + filename;
                        fileUpload.SaveAs(path);


                        var listCallPlan = ImportFacade.ReadCallPlan(Session["EmployeeID"].ToString(), path);

                        ImportFacade.DeleteMultipleCallPlan(listCallPlan);
                       
                        string result = ImportFacade.InsertCallPlan(listCallPlan);

                        if (result == "1")
                        {
                            alertSuccess();
                        }
                        else
                        {
                            
                            lblError.Text = result;
                        }
                         
                    }
                    else if(importType == "CUSTOMER")
                    {
                        string filename = "Customer.xlsx";
                        string path = Server.MapPath("~/import/") + filename;
                        fileUpload.SaveAs(path);

                        ImportFacade.DeleteCustomer();
                        var listCustomer = ImportFacade.ReadCustomer(Session["EmployeeID"].ToString(), path);
                        string result = ImportFacade.InsertCustomer(listCustomer);

                        if (result == "1")
                        {
                            alertSuccess();
                        }
                        else
                        {
                            lblError.Text = result;
                        }
                           
                    }
                    else if (importType == "EMPLOYEE")
                    {
                        string filename = "Employee.xlsx";
                        string path = Server.MapPath("~/import/") + filename;
                        fileUpload.SaveAs(path);

                        ImportFacade.DeleteEmployee();
                        var list = ImportFacade.ReadEmployee(Session["EmployeeID"].ToString(), path);
                        string result = ImportFacade.InsertEmployee(list);

                        if (result == "1")
                        {
                            alertSuccess();
                        }
                        else
                        {
                            lblError.Text = result;
                        }
                    }
                    else if (importType == "PRODUCT")
                    {
                        string filename = "Product.xlsx";
                        string path = Server.MapPath("~/import/") + filename;
                        fileUpload.SaveAs(path);

                        ImportFacade.DeleteProduct();
                        var list = ImportFacade.ReadProduct(Session["EmployeeID"].ToString(), path);
                        string result = ImportFacade.InsertProduct(list);

                        if (result == "1")
                        {
                            alertSuccess();
                        }
                        else
                        {
                            lblError.Text = result;
                        }
                    }
                    else if (importType == "COMPETITOR")
                    {
                        string filename = "Competitor.xlsx";
                        string path = Server.MapPath("~/import/") + filename;
                        fileUpload.SaveAs(path);

                        ImportFacade.DeleteCompetitor();
                        var list = ImportFacade.ReadCompetitor(Session["EmployeeID"].ToString(), path);
                        string result = ImportFacade.InsertCompetitor(list);

                        if (result == "1")
                        {
                            alertSuccess();
                        }
                        else
                        {
                            lblError.Text = result;
                        }
                    }
                    else if (importType == "COMPETITORACTIVITY")
                    {
                        string filename = "CompetitorActivity.xlsx";
                        string path = Server.MapPath("~/import/") + filename;
                        fileUpload.SaveAs(path);

                        ImportFacade.DeleteCompetitorActivity();
                        var list = ImportFacade.ReadCompetitorActivity(Session["EmployeeID"].ToString(), path);
                        string result = ImportFacade.InsertCompetitorActivity(list);

                        if (result == "1")
                        {
                            alertSuccess();
                        }
                        else
                        {
                            lblError.Text = result;
                        }
                    }
                    else if (importType == "COMPETITORPRODUCT")
                    {
                        string filename = "CompetitorProduct.xlsx";
                        string path = Server.MapPath("~/import/") + filename;
                        fileUpload.SaveAs(path);

                        ImportFacade.DeleteCompetitorProduct();
                        var list = ImportFacade.ReadCompetitorProduct(Session["EmployeeID"].ToString(), path);
                        string result = ImportFacade.InsertCompetitorProduct(list);

                        if (result == "1")
                        {
                            alertSuccess();
                        }
                        else
                        {
                            lblError.Text = result;
                        }
                    }
                    else if (importType == "POSMPRODUCT")
                    {
                        string filename = "POSMProduct.xlsx";
                        string path = Server.MapPath("~/import/") + filename;
                        fileUpload.SaveAs(path);

                        ImportFacade.DeletePOSMProduct();
                        var list = ImportFacade.ReadPOSMProduct(Session["EmployeeID"].ToString(), path);
                        string result = ImportFacade.InsertPOSMProduct(list);

                        if (result == "1")
                        {
                            alertSuccess();
                        }
                        else
                        {
                            lblError.Text = result;
                        }
                    }
                    else if (importType == "PROMO")
                    {
                        string filename = "Promo.xlsx";
                        string path = Server.MapPath("~/import/") + filename;
                        fileUpload.SaveAs(path);

                        ImportFacade.DeletePromo();
                        var list = ImportFacade.ReadPromo(Session["EmployeeID"].ToString(), path);
                        string result = ImportFacade.InsertPromo(list);

                        if (result == "1")
                        {
                            alertSuccess();
                        }
                        else
                        {
                            lblError.Text = result;
                        }
                    }
                    
                    

                //}
                //catch(Exception er)
                //{
                //    Response.Write("<script>alert('"+er.ToString()+"');</script>");
                //}

            }
        }

    }
}