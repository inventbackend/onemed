﻿/* documentation
 * 001 fernandes - 21 jul 2016
 * 002 nanda - 22 jul 2016
 * 003 nanda - 24 Ags 2016
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using System.Configuration;

namespace TransportService
{
    public partial class SendDOXMLForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            //001
            txtarriveTime.Text = "";
            txtshipDate.Text = "";
            txtshipFrom.Text = "";
            txtshipperNo.Text = "";
            txtvehRef.Text = "";
            //001
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            //002
            int lResendTimes = Convert.ToInt32(ConfigurationManager.AppSettings["ResendTimes"]);

            string lhasil = "";
            lhasil = DeliveryOrderFacade.SendDeliveryOrder(txtshipFrom.Text, txtshipperNo.Text, txtshipDate.Text, txtvehRef.Text, txtarriveTime.Text, ddlAutoInv.SelectedValue, ddlAutoPost.SelectedValue); //003
            if (lhasil == "RESEND")
            {
                for (int i = 0; i < lResendTimes ; i++)
                {
                    lhasil = DeliveryOrderFacade.SendDeliveryOrder(txtshipFrom.Text, txtshipperNo.Text, txtshipDate.Text, txtvehRef.Text, txtarriveTime.Text, ddlAutoInv.SelectedValue, ddlAutoPost.SelectedValue); //003
                    if (lhasil != "RESEND")
                    {
                        Response.Write("<script>alert('" + lhasil + "');</script>");
                        return;
                    }
                    Response.Write("<script>alert('" + lhasil + "');</script>");
                }
            }
            else
            {
                Response.Write("<script>alert('" + lhasil + "');</script>");
            }
            //002
        }


    }
}