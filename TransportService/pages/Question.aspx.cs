﻿/* documentation
 *001 nanda - 13 Okt 2016 
 *002 nanda - 21 Okt 2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using Core.Model;
using System.Collections;

namespace TransportService.pages
{
    public partial class Questions : System.Web.UI.Page
    {
        public Core.Model.User vUser;
        public string lParam { get; set; }

        public bool getRole()
        {
            Globals.gUserId = "";
            Globals.gRoleId = "";

            //get user accesssrole
            Globals.gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(Globals.gUserId);
            Globals.gRoleId = vUser.RoleID;
            string menuName = "Question";
            var vRole = UserFacade.CheckUserLeverage(Globals.gRoleId, menuName);

            return vRole;
        }

        private void ClearContent()
        {
            BindData();

            txtNo.Text = "";
            txtQuestionID.Value = "";
            txtQuestionText.Text = "";
            cbMandatory.Checked = false;
 
            PanelFormQuestion.Visible = false;
        }

        private void BindData()
        {
            var mdlQuestionList = QuestionFacade.LoadQuestion(Globals.gQuestionSetID, false, Boolean.Parse(ddlIsactive.SelectedValue));

            RptQuestionlist.DataSource = mdlQuestionList;
            RptQuestionlist.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var dsAnswerType = AnswerTypeFacade.LoadAnswerTypeDDL();
                ddlAnswerType.DataSource = dsAnswerType;
                ddlAnswerType.DataTextField = "AnswerTypeText";
                ddlAnswerType.DataValueField = "AnswerTypeID";
                ddlAnswerType.DataBind();

                ddlQuestionCategory.DataSource = QuestionCategoryFacade.LoadQuestionCategoryDDL(true);
                ddlQuestionCategory.DataTextField = "QuestionCategoryText";
                ddlQuestionCategory.DataValueField = "QuestionCategoryID";
                ddlQuestionCategory.DataBind();

                string[] controlArray = { "True", "False" };
                ddlIsactive.DataSource = controlArray;
                ddlIsactive.DataBind();

                ClearContent();
                var qsQuestionSetID = Request.QueryString["QuestionSetID"];
                var qsQuestionSetText = Request.QueryString["QuestionSetText"];

                if (qsQuestionSetID != null)
                {
                    Globals.gQuestionSetID = qsQuestionSetID;
                }

                if (qsQuestionSetText != null)
                {
                    Globals.gQuestionSetText = qsQuestionSetText;
                    
                }

                lblQuestionSet.Text = Globals.gQuestionSetText;
                BindData();
                
            }
        }

        protected void RptQuestionlist_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            var dtSequence = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtSequence")).Text);
            var dtNo = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtNo")).Text);
            var dtQuestionID = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtQuestionID")).Text);
            var dtQuestionText = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtQuestionText")).Text);
            var dtQuestionCategoryID = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtQuestionCategoryID")).Text);
            var dtAnswerTypeID = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtAnswerTypeID")).Text);
            var dtMandatory = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtMandatory")).Text);
            //var dtIsActive = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtIsActive")).Text);
            var dtIsMultiple = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtIsMultiple")).Text);

            if (e.CommandName == "Answer")
            {
                String Url = string.Format("Answer.aspx?QuestionID={0}&QuestionText={1}&IsMultiple={2}", dtQuestionID, dtQuestionText, dtIsMultiple);
                Response.Redirect(Url);
            }

            if (e.CommandName == "Update")
            {
               
                txtNo.Text = dtNo;
                txtQuestionID.Value = dtQuestionID;
                txtQuestionText.Text = dtQuestionText;
                ddlQuestionCategory.SelectedValue = dtQuestionCategoryID;
                ddlAnswerType.SelectedValue = dtAnswerTypeID;
                cbMandatory.Checked = Boolean.Parse(dtMandatory);
                txtSeq.Text  = dtSequence;


                btnInsert.Visible = false;
                btnUpdate.Visible = true;

                PanelFormQuestion.Visible = true;

            }

            if (e.CommandName == "Delete")
            {
                QuestionFacade.NonActiveQuestion(dtQuestionID);
                ClearContent();

                BindData();
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            var lmdlQuestion = new mdlQuestion();
            lmdlQuestion.AnswerID = "";
            lmdlQuestion.AnswerTypeID = ddlAnswerType.SelectedValue;
            lmdlQuestion.IsSubQuestion = false;
            lmdlQuestion.Mandatory = cbMandatory.Checked;
            lmdlQuestion.No = txtNo.Text;
            lmdlQuestion.QuestionCategoryID = ddlQuestionCategory.SelectedValue;
            lmdlQuestion.QuestionID = txtQuestionID.Value;
            lmdlQuestion.QuestionText = txtQuestionText.Text;
            lmdlQuestion.QuestionSetID = Globals.gQuestionSetID;
            lmdlQuestion.Sequence = Int32.Parse(txtSeq.Text);
            lmdlQuestion.IsActive = true;

            String lResult = QuestionFacade.UpdateQuestion(lmdlQuestion);
            ClearContent();

            BindData();
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            String lResult = "";
            var lmdlQuestion = new mdlQuestion();
            lmdlQuestion.AnswerID = "";
            lmdlQuestion.AnswerTypeID = ddlAnswerType.SelectedValue;
            lmdlQuestion.IsSubQuestion = false;
            lmdlQuestion.Mandatory = cbMandatory.Checked;
            lmdlQuestion.No = txtNo.Text;
            lmdlQuestion.QuestionCategoryID = ddlQuestionCategory.SelectedValue;
            lmdlQuestion.QuestionID = txtQuestionID.Value;
            lmdlQuestion.QuestionText = txtQuestionText.Text;
            lmdlQuestion.QuestionSetID = Globals.gQuestionSetID;
            lmdlQuestion.Sequence = Int32.Parse(txtSeq.Text);
            lmdlQuestion.IsActive = true;

            var lCheck = AnswerTypeFacade.CheckAnswer(ddlAnswerType.SelectedValue);
            if (lCheck == true)
            {
                lResult = QuestionFacade.InsertQuestion(lmdlQuestion);
            }
            else
            {
                var lmdlAnswer = new mdlAnswer();
                lmdlAnswer.AnswerID = ""; //generate by system
                lmdlAnswer.AnswerText = ddlAnswerType.SelectedItem.Text.Trim().Split('-')[2];
                lmdlAnswer.IsActive = true;
                lmdlAnswer.IsSubQuestion = false;
                lmdlAnswer.No = "1";
                lmdlAnswer.QuestionID = txtQuestionID.Value;
                lmdlAnswer.Sequence = Int32.Parse(txtSeq.Text);
                lmdlAnswer.SubQuestion = false;

                lResult = QuestionFacade.InsertQuestionNotMultiple(lmdlQuestion,lmdlAnswer,false ,"");
            }
                    
            ClearContent();

            BindData();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearContent();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            ClearContent();
            PanelFormQuestion.Visible = true;
            txtQuestionID.Value = QuestionFacade.GenerateQuestionID(false);

            btnInsert.Visible = true;
            btnUpdate.Visible = false;
        }

        protected void ddlIsactive_TextChanged(object sender, EventArgs e)
        {
            BindData();
        }
    }
}