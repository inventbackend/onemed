﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;

namespace TransportService.pages
{
    public partial class ReportOOSAccountBranch1 : System.Web.UI.Page
    {
        private List<Core.Model.mdlOOSProduct> listAccountProductOOS;
        private List<int> listWeek;
        private List<Core.Model.mdlOOS> listAccountOOS;
        private int year;
        private string branchID;
        private string role;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int fromWeek = Convert.ToInt32(Request.QueryString["fromweek"]);
                int toWeek = Convert.ToInt32(Request.QueryString["toweek"]);
                branchID = Request.QueryString["branchid"];
                role = Request.QueryString["role"];
                year = Convert.ToInt32(Request.QueryString["year"]);

                var listAcc = AccountFacade.GetAccount(branchID);
                    var listAccFinal = new List<Core.Model.mdlAccount>();

                    string account = "";

                    foreach (var item in listAcc)
                    {

                        var mdlAcc = new Core.Model.mdlAccount();
                        mdlAcc.AccountID = item.AccountID;
                        mdlAcc.AccountName = item.AccountName;
                        mdlAcc.Description = item.Description;
                        mdlAcc.Link = "ReportOOSAccountBranchDetail.aspx?fromweek=" + fromWeek + "&toweek=" + toWeek + "&account=" + mdlAcc.AccountID + "&branchid=" + branchID + "&year=" + year + "&role=" + role;
                        listAccFinal.Add(mdlAcc);
                            if (item == listAcc[listAcc.Count - 1])
                            {
                                account += item.AccountID;
                            }
                            else
                            {
                                account += item.AccountID + ",";
                            }
                    }

                listAccountOOS = OOSFacade.GetReportOOSByAccountBranch(fromWeek, toWeek, account,branchID, year,role);

                listWeek = listAccountOOS.Select(fld => fld.VisitWeek).Distinct().ToList();

                rptHeaderReportAccount.DataSource = listWeek;
                rptHeaderReportAccount.DataBind();

                rptParentReportAccount.DataSource = listAccFinal;
                rptParentReportAccount.DataBind();


                var listChartAccount = new List<Core.Model.mdlOOSChart>();
                int chartID = 1;

              
                foreach (var acc in listAccFinal)
                {
                    var tempListOOS = listAccountOOS.Where(fld => fld.Account.Equals(acc.AccountID));
                    //var listChartBranchData = new List<Core.Model.mdlOOSChartData>();
                    var listChartBranchData = tempListOOS.Select(fld => new { fld.VisitWeek, fld.OOS }).OrderBy(fld => fld.VisitWeek).ToList();

                    string json = Core.Services.RestPublisher.Serialize(listChartBranchData);
                    var chart = new Core.Model.mdlOOSChart();
                    chart.ID = "chart" + chartID.ToString();
                    chart.Area = acc.AccountID;
                    chart.json = json;
                    listChartAccount.Add(chart);

                   
                    chartID++;

                }

                rptBranchChart.DataSource = listChartAccount;
                rptBranchChart.DataBind();


                var listOOSGrandTotal = new List<decimal>();
                foreach (int i in listWeek)
                {
                    decimal totalOOS = listAccountOOS.Where(fld => fld.VisitWeek.Equals(i)).Sum(r => r.OutOfStock);
                    decimal totalListed = totalOOS + listAccountOOS.Where(fld => fld.VisitWeek.Equals(i)).Sum(r => r.Listed);
                    decimal average = 0;
                    if (totalListed != 0)
                    {
                        average = (totalOOS / totalListed) * 100;
                    }
                    listOOSGrandTotal.Add(decimal.Round(average, 2));
                }

                rptGrandTotal.DataSource = listOOSGrandTotal;
                rptGrandTotal.DataBind();

            }
        }

        protected void ReportAccountItemBound(object sender, RepeaterItemEventArgs args)
        {
            RepeaterItem ri = args.Item;
            if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater childParentAccountRepeater = (Repeater)args.Item.FindControl("rptChildAccountStock");
                Label lblAccount = ri.FindControl("lblAccount") as Label;

                var listAccount = listAccountOOS.Where(fld => fld.Account.Equals(lblAccount.Text) & fld.BranchID.Equals(branchID)).ToList();

                foreach (int i in listWeek)
                {
                    var temp = listAccount.FirstOrDefault(fld => fld.VisitWeek.Equals(i));
                    if (temp == null)
                    {
                        var newmodel = new Core.Model.mdlOOS();
                        newmodel.BranchID = branchID;
                        newmodel.Account = lblAccount.Text;
                        newmodel.Listed = 0;
                        newmodel.OutOfStock = 0;
                        newmodel.VisitWeek = i;
                        newmodel.OOS = 0;
                        listAccount.Add(newmodel);

                    }
                }

                listAccount = listAccount.OrderBy(fld => fld.VisitWeek).ToList();

                childParentAccountRepeater.DataSource = listAccount;
                childParentAccountRepeater.DataBind();

            }
        }
    }
}