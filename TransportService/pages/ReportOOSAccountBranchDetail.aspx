﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true" CodeBehind="ReportOOSAccountBranchDetail.aspx.cs" Inherits="TransportService.pages.ReportOOSAccountBranch" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<style>
.tablefixed {
  
  overflow-x: scroll;
 
  overflow-y: visible;
  padding-bottom: 1px;
}
.td {
  padding: 0 40px;
}
</style>
<script type="text/javascript">
    function Navigate() {

        window.history.go(-1);
    }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<asp:ScriptManager ID="ScriptManager2" runat="server">
                                    </asp:ScriptManager>
    <div class="page-title">
         <div class="title_left">
            <h3>
                Form Report OOS</h3>
                <div class="title_right">
                </div>
         </div>
          
    </div>
    <div class="clearfix">
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <br />
                    <div class="form-group col-md-12 col-xs-12 col-lg-12">
                        <div class="col-md-12 col-xs-12 col-lg-12">
                           <%-- <div class="row" style="margin-bottom:10px;">
                                <div class="col-xs-12 col-md-6">
                                    
                                    <asp:Label ID="lblWeekFrom" runat="server" Text="From Week" Width="100px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                    <asp:DropDownList ID="ddlWeekFrom" class="form-control" runat="server">
                                        
                                    </asp:DropDownList>
                                    
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <asp:Label ID="lblWeekTo" runat="server" Text="To Week" Width="100px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                    <asp:DropDownList ID="ddlWeekTo" class="form-control" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom:10px;">
                                <div class="col-xs-12 col-md-6">
                                    <asp:Label ID="Label1" runat="server" Text="By" Width="100px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                              
                                    <asp:DropDownList ID="ddlBy" runat="server" class="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlBy_SelectedIndexChanged">
                                        <asp:ListItem Text="AREA" Value="AREA"></asp:ListItem>
                                        <asp:ListItem Text="ACCOUNT" Value="ACCOUNT"></asp:ListItem>
                                    </asp:DropDownList>

                                </div>
                                <div class="col-xs-12 col-md-6">
                                <asp:Label ID="Label2" runat="server" Text="Year" Width="100px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                              
                                    <asp:DropDownList ID="ddlYear" runat="server" class="form-control" >
                                        
                                    </asp:DropDownList>
                                </div>
                            </div>--%>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <%--<asp:Button ID="btnback" runat="server" Text="Back" class="btn btn-sm btn-embossed btn-primary"
                                        Font-Size="14px" OnClientClick="Navigate()"/>--%>
                                        <input action="action" type="button" value="Back" class="btn btn-sm btn-embossed btn-primary"  onclick="window.history.go(-1);" />
                                </div>
                            </div>
                            <div class="row" style="margin-bottom:10px;">
                                <div class="col-xs-12 col-md-6">
                                   <%-- <div class="row" style="margin-bottom:10px;">
                                        <div class="col-xs-12 col-md-6">
                                            <asp:Label ID="lblAreaOrAccount" runat="server" Text="Account" Width="100px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                        </div>
                                       
                                    </div>
                                    <div class="row" style="margin-bottom:10px;">
                                        <div class="col-xs-12 col-md-7">
                                        
                                            <div class="col-xs-12 col-md-12" style="overflow-y:scroll;height:300px;">
                                                

                                                <asp:CheckBoxList ID="cblAccount" runat="server">
                                                </asp:CheckBoxList>
                                                
                                            </div>
                                            <div class="row" style="margin-bottom:10px;">
                                                <div class = "col-xs-12 col-md-5">
                                                    <asp:Button ID="btnSelectAll" runat="server" CssClass="btn btn-success" Text="SELECT ALL" onclick="btnSelectAll_Click" style="margin-left:10px"/>
                                                
                                                </div>
                                                <div class = "col-xs-12 col-md-6">
                                                    <asp:Button ID="btnUnSelectAll" runat="server" CssClass="btn btn-success " Text="UNSELECT ALL" onclick="btnUnSelectAll_Click" style="margin-left:10px"/>
                                                </div>
                                                
                                            </div>
                                       
                                        </div>
                                        
                                        
                                    </div>
                                    <div class="row">   
                                        <div class="col-md-12 col-xs-12">
                                            <asp:Button ID="btnShow" runat="server" Text="Show Report" class="btn btn-sm btn-embossed btn-primary"
                                        Font-Size="14px" OnClick="btnShow_Click" />
                                        <asp:Button ID="btnShowExcel" runat="server" Text="Show Report Excel" class="btn btn-sm btn-embossed btn-primary"
                                        Font-Size="14px" OnClick="btnShowExcel_Click" ValidationGroup="valShowReport" />
                                        </div>
                                        


                                    </div>--%>

                                    <div class="row">
                                        
                                       <%-- <div class="col-md-12 datagrid tablefixed" runat="server" id="divTable">
                                            
                                            <table style="width:100%;margin-bottom: 0px;">

                                                <asp:Repeater ID="rptHeader" runat="server">
                                                <HeaderTemplate>
                                                <thead>
                                                    <th>
                                                      
                                                             <asp:Label ID="lblHeader" runat="server" Text='AREA'></asp:Label>
                                                      </th>
                                                </HeaderTemplate>
                                                    
                                                <ItemTemplate>
                                                    <th>
                                                        <asp:Label ID="lWeek" runat="server" Text='<%# Container.DataItem %>'></asp:Label>
                                                    </th>
                                                </ItemTemplate>
                                                    <FooterTemplate>
                                                    
                                                    
                                                    </tr>
                                                    </thead>
                                                    </FooterTemplate>
                                                </asp:Repeater>

                                                <asp:Repeater ID="rptparent" runat="server" OnItemDataBound="ItemBound">
                                                    
                                                     <ItemTemplate>
                                                        <tbody>
                                                        <tr bgcolor="#ADADAD">
                                                        <td  style="position:absolute; left:0;">
                                                            <asp:Label ID="lblArea" runat="server" Text='<%# Container.DataItem %>'></asp:Label>
                                                        </td>
                                                        <asp:Repeater ID="rptChild" runat="server">
                                                            <ItemTemplate>
                                                                <td>
                                                                    <asp:Label ID="lblData" runat="server" Text='<%# Eval("OOS") %>'></asp:Label>%
                                                                </td>
                                                            </ItemTemplate>
                                                        </asp:Repeater>


                                                        </tr>
                                                        
                                                            
                                                            <asp:Repeater ID="rptParentAccount" runat="server" OnItemDataBound="AccountItemBound">
                                                                <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblDataParenAccount" runat="server" Text='<%# Eval("AccountID") %>'></asp:Label>
                                                                    </td>
                                                                    <asp:Repeater ID="rptChildAccount" runat="server">
                                                                        <ItemTemplate>
                                                                            <td>
                                                                                <asp:Label ID="lblDataChildAccount" runat="server" Text='<%# Eval("OOS") %>'></asp:Label>%
                                                                             </td>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                   </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                            <tr >
                                                            
                                                                <td style="font-weight:bold">Total</td>
                                                                <asp:Repeater ID="rptTotal" runat="server">
                                                                    <ItemTemplate>
                                                                    <td style="font-weight:bold">
                                                                        <asp:Label ID="lblTotal" runat="server" Text='<%# Container.DataItem %>'></asp:Label>%
                                                                    </td>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </tr>
                                                       
                                                        </tbody>
                                                     </ItemTemplate>
                                                </asp:Repeater>
                                                    <tbody>
                                                        <tr >
                                                            <td style="font-weight:bolder;"><asp:Label ID="lbltot" runat="server" Text='GRAND TOTAL'></asp:Label></td>
                                                            <asp:Repeater ID="rptGrandTotal" runat="server">
                                                                <ItemTemplate>
                                                                    <td style="font-weight:bolder;">
                                                                        <asp:Label ID="lblGrandTotal" runat="server" Text='<%# Container.DataItem %>'></asp:Label>%
                                                                    </td>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </tr>
                                                    </tbody>
                                            
                                            
                                            </table>

                                        </div>--%>
                                        

                                        <div class="col-md-12 datagrid tablefixed" runat="server" id="divTableAccount">
                                            
                                            <table style="width:100%;margin-bottom: 0px;">

                                                <asp:Repeater ID="rptHeaderReportAccount" runat="server">
                                                <HeaderTemplate>
                                                <thead>
                                                    <th>
                                                      
                                                             <asp:Label ID="lblHeader" runat="server" Text='ACCOUNT'></asp:Label>
                                                      </th>
                                                      <th>
                                                      
                                                             <asp:Label ID="Label3" runat="server" Text='Product ID'></asp:Label>
                                                      </th>
                                                </HeaderTemplate>
                                                    
                                                <ItemTemplate>
                                                    <th>
                                                        <asp:Label ID="lWeek" runat="server" Text='<%# Container.DataItem %>'></asp:Label>
                                                    </th>
                                                </ItemTemplate>
                                                    <FooterTemplate>
                                                    
                                                    
                                                    </tr>
                                                    </thead>
                                                    </FooterTemplate>
                                                </asp:Repeater>

                                                <asp:Repeater ID="rptParentReportAccount" runat="server" OnItemDataBound="ReportAccountItemBound">
                                                    
                                                     <ItemTemplate>
                                                        <tbody>
                                                        <tr>
                                                        <td  style=" left:0;">
                                                            <asp:Label ID="lblAccount" runat="server" Text='<%# Eval("Account") %>'></asp:Label>
                                                        </td>
                                                        <td  style=" left:0;">
                                                            <asp:Label Visible="false" ID="lblProductID" runat="server" Text='<%# Eval("ProductID") %>'></asp:Label>
                                                            <asp:Label ID="lblProductName" runat="server" Text='<%# Eval("ProductName") %>'></asp:Label>
                                                        </td>
                                                        <asp:Repeater ID="rptChildAccountStock" runat="server">
                                                            <ItemTemplate>
                                                                <td>
                                                                    <a href='<%# Eval("Link") %>'><asp:Label ID="lblData" runat="server" Text='<%# Eval("OOSCustomer") %>'></asp:Label></a>
                                                                </td>
                                                            </ItemTemplate>
                                                        </asp:Repeater>


                                                        </tr>
                                                        
                                                            
                                                            <%--<asp:Repeater ID="rptParentAccount" runat="server" OnItemDataBound="AccountItemBound">
                                                                <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblDataParenAccount" runat="server" Text='<%# Eval("AccountID") %>'></asp:Label>
                                                                    </td>
                                                                    <asp:Repeater ID="rptChildAccount" runat="server">
                                                                        <ItemTemplate>
                                                                            <td>
                                                                                <asp:Label ID="lblDataChildAccount" runat="server" Text='<%# Eval("OOS") %>'></asp:Label>%
                                                                             </td>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                   </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>--%>
                                                            
                                                       
                                                        </tbody>
                                                     </ItemTemplate>
                                                </asp:Repeater>
                                            
                                            
                                            </table>

                                        </div>

                                    </div>

                                    <%--<div class="row">
                                        <div class="col-md-12 col-xs-12">
                                            <asp:Panel ID="PanelReportOOS" runat="server" Height="500px" Width="100%" CssClass="datagrid">
                                                <rsweb:ReportViewer ID="reportOOS" runat="server" Font-Names="Verdana" Font-Size="8pt"
                                                    Width="100%" Height="500px" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana"
                                                    WaitMessageFont-Size="14pt">
                                                    <LocalReport ReportPath="Reports\ReportOOS.rdlc">
                                                    </LocalReport>
                                                </rsweb:ReportViewer>
                                            </asp:Panel>
                                        </div>
                                    
                                    </div>--%>
                                    
                                </div>
                                
                            </div>
                            
                         </div>
                       
                    </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
</asp:Content>
